/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author diogo
 */
public class MealMenuPrinter implements Visitor<MealMenu>{
    @Override
    public void visit(MealMenu visitee) {
        System.out.printf("%-10s%-30s%-4s\n", visitee.id(), visitee.descricao(), String.valueOf(visitee.isActive()));
    }

    @Override
    public void beforeVisiting(MealMenu visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(MealMenu visitee) {
        // nothing to do
    }
}
