/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.ecafeteria.utente.consoleapp.presentation.authz.AddReservationUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.ShowUiAction;

/**
 *
 * @author Leandro Sousa <1140350>
 */
public class MakeReservationsUI extends Menu {

    private final int ADD_RESERVATION = 1;
    private final int EXIT_OPTION = 0;
    private String date;

    MakeReservationsUI() {
        super("Make Reservation");
        this.makeReservation();
    }

    public void makeReservation() {
        add(new MenuItem(ADD_RESERVATION, "Add Reservation", new ShowUiAction(new AddReservationUI())));
        add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

    }

}
