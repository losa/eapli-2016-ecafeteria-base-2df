/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Leandro Sousa <1140350>
 */
public class MealPrinter implements Visitor<Meal> {

    private static int nElem = 1;

    @Override
    public void visit(Meal visitee) {
        System.out.printf("%d - %-30s %s\n", nElem, visitee.dish().name(), visitee.mealType().description());
        nElem++;
    }

    @Override
    public void beforeVisiting(Meal visitee) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void afterVisiting(Meal visitee) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
