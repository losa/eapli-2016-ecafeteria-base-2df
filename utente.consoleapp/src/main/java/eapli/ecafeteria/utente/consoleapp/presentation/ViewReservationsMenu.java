/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.ecafeteria.utente.consoleapp.presentation.authz.ListFutureReservationsUI;
import eapli.ecafeteria.utente.consoleapp.presentation.authz.ListPreviousReservationsUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.ShowUiAction;

/**
 *
 * @author SIGMA
 */
public class ViewReservationsMenu extends Menu{
    //VIEWING OPTIONS
    private static final int PREVIOUS_RESERVATIONS_OPTION=0;
    private static final int FUTURE_RESERVATIONS_OPTION=1;
    //TODO add new reservation viewing options

    public ViewReservationsMenu() {
        super("View Reservations");
        buildViewReservationsMenu();
    }
    
    //Builds the View Reservations Menu with any specified MenuItems
    //new reservation viewing options should be added here
    private void buildViewReservationsMenu(){
        add(new MenuItem(PREVIOUS_RESERVATIONS_OPTION, "View Previous Reservations", new ShowUiAction(new ListPreviousReservationsUI())));
        add(new MenuItem(FUTURE_RESERVATIONS_OPTION, "View Next Reservations", new ShowUiAction(new ListFutureReservationsUI())));
        //TODO add new reservation viewing options
    }
}
