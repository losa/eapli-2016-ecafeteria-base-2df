/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.ecafeteria.application.ListReservationsController;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author SIGMA
 */
public abstract class ListReservationsUI extends AbstractListUI<Reservation>{
    
    private final ListReservationsController theController = new ListReservationsController();

    public ListReservationsUI(){}
    

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected Iterable<Reservation> listOfElements() {
        //does nothing
        return null;
    }

    @Override
    protected Visitor<Reservation> elementPrinter() {
        return new ReservationPrinter();
    }
    
    @Override
    protected boolean doShow() {
        
        return super.doShow();
    }

    @Override
    protected String elementName() {
        return "Reservation";
    }
}
