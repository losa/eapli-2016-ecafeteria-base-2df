package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author SIGMA
 */
public class ReservationPrinter implements Visitor<Reservation>{
    public ReservationPrinter(){}

    @Override
    public void visit(Reservation visitee) {
        //TODO add visiting parameters to the status atribute of a Reservation
        System.out.printf("Meal: %s; Date: %s\n", visitee.Meal().toString(), visitee.Date());
    }

    @Override
    public void beforeVisiting(Reservation visitee) {
        //nothing to do
    }

    @Override
    public void afterVisiting(Reservation visitee) {
        //nothing to do
    }
    
}
