/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.framework.visitor.Visitor;


/**
 *
 * @author SIGMA
 */
public class ListFutureReservationsUI extends ListReservationsUI{
    public ListFutureReservationsUI(){}
    
    @Override
    //presents the information on screen
    //the reservations list differs from UI to UI
    public boolean doShow(){
        //TODO present a reservations list of all the future reservations
        return true;
    }
    
    @Override
    public String headline() {
        return "Future Reservations";
    }

    //TODO setup these 3 methods
    @Override
    protected Iterable listOfElements() {
        //TODO return the iterable of the list obtained with the method getFutureReservations(int days)
        //from the ViewReservationsController class
        return null;
    }

    @Override
    protected Visitor elementPrinter() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String elementName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
