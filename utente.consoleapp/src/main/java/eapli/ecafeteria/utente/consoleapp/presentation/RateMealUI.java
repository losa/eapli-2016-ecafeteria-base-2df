/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.ecafeteria.application.ListAvailableMenusController;
import eapli.ecafeteria.application.RateMealController;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Nuno
 */
public class RateMealUI extends AbstractListUI<Reservation> {

    private final RateMealController theController = new RateMealController();

    public RateMealUI() {
        super();
    }


    @Override
    protected Iterable<Reservation> listOfElements() {
       return this.theController.getCompleteReservationList();
    }

    @Override
    protected Visitor<Reservation> elementPrinter() {
        return new RatingReservationPrinter();
    }

    @Override
    protected String elementName() {
        return "Reservastions to rate";
    }
    
}
