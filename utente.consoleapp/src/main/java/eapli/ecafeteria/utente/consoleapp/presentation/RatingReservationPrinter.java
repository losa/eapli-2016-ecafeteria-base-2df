/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Rating;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Nuno Barbosa 1140372 SIGMA
 */
class RatingReservationPrinter implements Visitor<Reservation> {

    @Override
    public void visit(Reservation visitee) {
        System.out.printf("%-10s%-30s%-4s\n",  visitee.toString());
    }

    @Override
    public void beforeVisiting(Reservation visitee) {
        
    }

    @Override
    public void afterVisiting(Reservation visitee) {
        
    }
    
}
