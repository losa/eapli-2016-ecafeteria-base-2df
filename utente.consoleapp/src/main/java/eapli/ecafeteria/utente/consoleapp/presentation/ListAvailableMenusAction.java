/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.framework.actions.Action;

/**
 *
 * @author diogo
 */
public class ListAvailableMenusAction implements Action {
    @Override
    public boolean execute() {
        return new ListAvailableMenusUI().show();
    }
}
