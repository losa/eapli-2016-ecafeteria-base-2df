/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;

/**
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final int EXIT_OPTION = 0;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;

    private static final int VIEW_RESERVATIONS_OPTION = 2;

    private static final int VIEW_AVAILABLE_MENUS_OPTION = 3;

    private static final int MAKE_RESERVATIONS_OPTION = 4;
    
    private static final int CANCEL_RESERVATION_OPTION = 5;
    
    private static final int RATE_MEAL_OPTION = 6;

    /*
     * Option not being used private static final int DISH_TYPE_OPTION = 2;
     */
    public MainMenu() {
    }

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer = new VerticalMenuRenderer(menu);
        return renderer.show();
    }

    @Override
    public String headline() {
        return "eCAFETERIA [@" + AppSettings.instance().session().authenticatedUser().id() + "]";
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        mainMenu.add(VerticalSeparator.separator());

        // TODO add menu options
        mainMenu.add(new MenuItem(VIEW_RESERVATIONS_OPTION, "View Reservations", new ShowVerticalSubMenuAction(new ViewReservationsMenu())));

        mainMenu.add(new MenuItem(VIEW_AVAILABLE_MENUS_OPTION, "List all Available Menus", new ListAvailableMenusAction()));

        mainMenu.add(new MenuItem(MAKE_RESERVATIONS_OPTION, "Make Reservation", new ShowVerticalSubMenuAction(new MakeReservationsUI())));

        mainMenu.add(new MenuItem(CANCEL_RESERVATION_OPTION, "Cancel Reservation", new ShowVerticalSubMenuAction(new CancelReservations())));

        mainMenu.add(new MenuItem(RATE_MEAL_OPTION, "Rate a meal", new RateMealMenu()));
        mainMenu.add(VerticalSeparator.separator());

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));
        
        AppSettings app = AppSettings.instance();
        SystemUser a = app.session().authenticatedUser();
        CafeteriaUser user = PersistenceContext.repositories().cafeteriaUsers().findBySystemUser(a);
        System.out.println(user.Account().saldo());

//        AppSettings app = AppSettings.instance();
//        SystemUser a = app.session().authenticatedUser();
//        CafeteriaUser user = PersistenceContext.repositories().cafeteriaUsers().findBySystemUser(a);
//        System.out.println(user.Account().saldo());

        return mainMenu;
    }
}
