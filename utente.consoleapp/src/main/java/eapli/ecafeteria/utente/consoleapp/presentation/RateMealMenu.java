/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.ecafeteria.application.RateMealController;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.utente.consoleapp.presentation.authz.ListFutureReservationsUI;
import eapli.ecafeteria.utente.consoleapp.presentation.authz.ListPreviousReservationsUI;
import eapli.framework.actions.Action;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.ShowUiAction;

/**
 *
 * @author Nuno Barbosa 1140372 SIGMA
 */
public class RateMealMenu implements Action {

    @Override
    public boolean execute() {
        return new RateMealUI().show();
    }
    
   
    
}
