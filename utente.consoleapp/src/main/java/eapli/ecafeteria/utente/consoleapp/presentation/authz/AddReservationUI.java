/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation.authz;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.ecafeteria.application.MakeReservationsController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.utente.consoleapp.presentation.MealMenuPrinter;
import eapli.ecafeteria.utente.consoleapp.presentation.MealPrinter;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.Menu;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Leandro Sousa <1140350>
 */
public class AddReservationUI extends AbstractUI {

    private final MakeReservationsController controller = new MakeReservationsController();

    public AddReservationUI() {

    }

    public MakeReservationsController Controller() {
        return controller;
    }

    @Override
    protected boolean doShow() {
        Calendar date1;
        date1 = Console.readCalendar("Enter the day:(dd-MM-yyyy)");
        try {
           Iterable<Meal> meal = this.controller.chooseMeal(date1);
            List<Meal> m_listMeal = new ArrayList<>();
            MealPrinter printer = new MealPrinter();
            System.out.println("Escolha uma Opção:");
            for (Meal m : meal) {
                m_listMeal.add(m);
                printer.visit(m);
            }
            int option = Console.readOption(1, m_listMeal.size(), 0);
            this.controller.newReserva(m_listMeal.get(option - 1));

        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(AddReservationUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    @Override
    public String headline() {
        return ("Make Reservation");
    }

}
