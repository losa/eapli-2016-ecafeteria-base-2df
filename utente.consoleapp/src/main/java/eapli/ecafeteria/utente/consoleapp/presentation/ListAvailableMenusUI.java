/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.ecafeteria.application.ListAvailableMenusController;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author diogo
 */
public class ListAvailableMenusUI extends AbstractListUI<MealMenu> {
    
    private final ListAvailableMenusController theController = new ListAvailableMenusController();
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected Iterable<MealMenu> listOfElements() {
        return this.theController.listAvailableMenus();
    }

    @Override
    protected Visitor<MealMenu>elementPrinter() {
        return new MealMenuPrinter();
    }

    @Override
    protected String elementName() {
        return "Meal Menu";
    }
    
}
