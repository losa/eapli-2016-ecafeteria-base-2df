/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.MealsType;
import eapli.framework.application.Controller;

/**
 *
 * @author Bolt
 */
public class ListMealsTypeController implements Controller{
    
    public Iterable<MealsType> listMealsTypes() {
        return new ListMealsTypeService().allMealsTypes();
    }
}
