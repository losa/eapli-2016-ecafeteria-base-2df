/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.cafeteria.WorkTimeCashier;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.POSRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author RP
 */
public class ClosePOSController implements Controller {

    //ListReservationsController lr = new ListReservationsController();
    public boolean closePOS() {
        POS pointOfSale = getPOS();
        //CONFIRMA SE EXISTE CAIXA ACTIVA
        if (pointOfSale != null) {
            //TENTA FECHAR A CAIXA
            if (!tryClosePOS(pointOfSale)) {
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                WorkTimeCashier wtcNow;
                Calendar now = Calendar.getInstance();
                if (now.get(Calendar.HOUR_OF_DAY) < 15) {
                    wtcNow = WorkTimeCashier.ALMOCO;
                } else {
                    wtcNow = WorkTimeCashier.JANTAR;
                }

                Iterable<Reservation> listDay = listReserveDay(dateFormat, now, wtcNow);
                //CONFIRMA SE É A ULTIMA POS ACTIVA
                if (lastPOS(dateFormat, now, wtcNow)) {
                    if (listDay != null) {
                        efectivarReservation(listDay, dateFormat, now, wtcNow);
                    }
                    int summary[] = summary(dateFormat, now, wtcNow);
                    System.out.println("SUMMARY FOR THIS POINT OF SALE:\n Last hour sales: " + pointOfSale.getVendasUltimaHora()
                            + ".\n Meals Delivery: " + pointOfSale.getRefeicoesEntregues() + ".\n");
                    System.out.println("SUMMARY TOTAL:\n Last hour sales: " + summary[0] + ".\n Meals Delivery: " + summary[1] + "\n");
                    return true;
                } else {
                    System.out.println("There are more open points of sale in progress.");
                    System.out.println("SUMMARY FOR THIS POINT OF SALE:\n Last hour sales: " + pointOfSale.getVendasUltimaHora() + ".\n Meals Delivery: "
                            + pointOfSale.getRefeicoesEntregues() + ".\n");
                    return true;
                }
            } else {
                System.out.println("Error when trying to close point of sale");
            }
        } else {
            System.out.println("None point of sale open.");
        }
        return false;
    }

    public Iterable<Reservation> listReserveDay(DateFormat dateFormat, Calendar now, WorkTimeCashier wtcNow) {
        Iterable<Reservation> listDay = PersistenceContext.repositories().reservations().all();
        if (listDay.iterator().hasNext()) {
            List<Reservation> listday = new ArrayList<>();
            WorkTimeCashier wtc;
            Calendar date;
            for (Reservation reservation : listDay) {
                date = reservation.getDate();
                if (date.get(Calendar.HOUR_OF_DAY) < 15) {
                    wtc = WorkTimeCashier.ALMOCO;
                } else {
                    wtc = WorkTimeCashier.JANTAR;
                }
                if (dateFormat.format(now.getTime()).equals(dateFormat.format(date.getTime()))
                        && wtc.equals(wtcNow)) {
                    listday.add(reservation);
                }
            }
            return listday;
        }
        return null;
    }

    public boolean lastPOS(DateFormat dateFormat, Calendar now, WorkTimeCashier wtcNow) {
        final POSRepository POSRepository = PersistenceContext.repositories().pointOfSale();
        Iterable<POS> POs = POSRepository.all();
        for (POS pos : POs) {
            //COMPARA DATA ACTUAL COM A DATA DA POS E O HORARIO DE TRABALHO
            if (dateFormat.format(now.getTime()).equals(dateFormat.format(pos.getDay().getTime())) && pos.Hour() == wtcNow) {
                if (pos.isActive() == true) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean efectivarReservation(Iterable<Reservation> listDay, DateFormat dateFormat, Calendar now, WorkTimeCashier wtcNow) {
        Map<Meal, Integer> m = new HashMap();
        //MUDA ESTADO DAS RESERVAS CASO SEJA A ULTIMA CAIXA
        for (Reservation reservation : listDay) {
            if (reservation.status() == Reservation.enumStatus.WAITING) {
                reservation.rejectReservation();
            }
            if (reservation.status() == Reservation.enumStatus.DELIVERY) {
                Integer n = m.getOrDefault(reservation.Meal(), 0);
                n = n + 1;
                m.put(reservation.Meal(), n);
            }
        }

        try {
            Iterable<MealPlanItem> it = PersistenceContext.repositories().mealPlanItem().all();
            for (MealPlanItem item : it) {
                Integer i = m.get(item.meal());
                if (i != null) {
                    item.undeliveredQtt(item.cookedQtt() - i);
                }
            }
        } catch (Exception e) {

        }

        return true;
    }

    public int[] summary(DateFormat dateFormat, Calendar now, WorkTimeCashier wtcNow) {
        int vec[] = new int[2];
        vec[0] = 0;
        vec[1] = 0;

        final POSRepository POSRepository = PersistenceContext.repositories().pointOfSale();
        Iterable<POS> list_POS = POSRepository.all();
        for (POS pos : list_POS) {
            //CONFIRMA SE A CAIXA É DO DIA
            if (dateFormat.format(now.getTime()).equals(dateFormat.format(pos.getDay().getTime())) && pos.Hour() == wtcNow) {
                vec[0] += pos.getVendasUltimaHora();
                vec[1] += pos.getRefeicoesEntregues();
            }
        }
        return vec;
    }

    public POS getPOS() {
        SystemUser user = AppSettings.instance().session().authenticatedUser();
        Iterable<POS> listPos = PersistenceContext.repositories().pointOfSale().all();
        Iterator it = listPos.iterator();
        while (it.hasNext()) {
            POS pointOfSale = (POS) it.next();
            if (pointOfSale.isActive() == true) {
                if (pointOfSale.id().equals(user)) {
                    return pointOfSale;
                }
            }
        }
        return null;
    }

    public boolean tryClosePOS(POS pointOfSale) {
        pointOfSale.toInactive();
        PersistenceContext.repositories().pointOfSale().save(pointOfSale);
        return pointOfSale.isActive();
    }
}
