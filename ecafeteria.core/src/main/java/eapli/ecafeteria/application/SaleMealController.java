/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.Console;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author andreiavilarinho
 */
public class SaleMealController implements Controller {

    public boolean saleMeal() throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.Sale);
        SystemUser user = AppSettings.instance().session().authenticatedUser();
        POS pointOfSale = getPOS(user);
        if (pointOfSale != null) {
            if (chooseMeal()) {
                pointOfSale.addVendasUltimaHora();
                return true;
            }
        }else{
            System.out.println("Point of Sale was not open yet.");
        }
        return false;
    }

    public boolean chooseMeal() throws DataIntegrityViolationException {
        ListAvailableDishesController ladController = new ListAvailableDishesController();
        Map<Integer, MealPlanItem> map_MealPlanItem = ladController.availableDishes();

         if (!map_MealPlanItem.isEmpty()) {
            Set<Integer> key = map_MealPlanItem.keySet();
            System.out.println("KEY: " +key +"\n");
            for (Iterator<Integer> iterator = key.iterator(); iterator.hasNext();) {
                Integer k = iterator.next();
                System.out.print("KEY: "+ k);
                if (k != null) {
                    System.out.println(" - Dish: " + map_MealPlanItem.get(k).meal().dish().dishType() + " Ammount: " + map_MealPlanItem.get(k).avaiableQtt());
                }
            }
            //SELECIONA A REFEICAO
            int op = Console.readInteger("Option: ");
            map_MealPlanItem.get(op);
            //SE CONSEGUIR PAGAR ENTAO DECREMENTA
            if (payMeal(map_MealPlanItem.get(op).meal())) {
                map_MealPlanItem.get(op).decAvaiableQtt();
                System.out.println("Payment done.\n");
                return true;
            } else {
                System.out.println("An error occurred in the payment.\n");
            }
        }
        System.out.println("None dish available.\n");
        return false;
    }

    public boolean payMeal(Meal meal) throws DataIntegrityViolationException {

        final int op = Console.readInteger("(1) Registered User\n(2) No Registered User");
        switch (op) {
            case 1:
                AccountChargeController acc = new AccountChargeController();
                final String number = Console.readLine("Number of Client: ");
                MecanographicNumber mecNumber = new MecanographicNumber(number);
                Account account = acc.getAccountID(mecNumber);
                if (account != null) {
                    final int op2 = Console.readInteger("\n(1) Account\n(2) MB\n Option: ");
                    if (op2 == 2) {
                        return lastSaleAccount(acc, account, meal.dish().price());
                    } else {
                        return lastSaleMB();
                    }
                } else {
                    System.out.println("No user registed to that number");
                }
                break;
            case 2:
                return lastSaleMB();
            default:
                System.out.println("Invalid Option.");
                break;
        }
        return false;
    }

    public boolean lastSaleAccount(AccountChargeController acc, Account account, String price) throws DataIntegrityViolationException {
        double priceD = Double.parseDouble(price);
        return acc.chargeAccount(-priceD, account);
    }

    public boolean lastSaleMB() {
        return true;
    }

    public POS getPOS(SystemUser user) {
        Iterable<POS> listPos = PersistenceContext.repositories().pointOfSale().all();
        Iterator it = listPos.iterator();
        while (it.hasNext()) {
            POS pointOfSale = (POS) it.next();
            if (pointOfSale.isActive()) {
                if (pointOfSale.id().equals(user)) {
                    return pointOfSale;
                }
            }
        }
        return null;
    }
}
