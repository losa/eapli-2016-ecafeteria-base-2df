/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.UnauthorizedException;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author diogo
 */
public class ListAvailableMenusService {
    public Iterable<MealMenu> availableMenus() {
        try{
            ensurePermissionOfLoggedInUser(ActionRight.CheckMenus);
        }catch(UnauthorizedException e){
            System.out.println("User is not authorized to perform this action! \n"+AppSettings.instance().session().authenticatedUser()+"\n"+ActionRight.ManageMenus+"\n");
        }

        final MealMenuRepository mealMenuRepository = PersistenceContext.repositories().mealMenu();
        return mealMenuRepository.activeMealMenu();
    }

    Iterable<MealMenu> allMenus() {
        try{
            ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        }catch(UnauthorizedException e){
            System.out.println("User is not authorized to perform this action! \n"+AppSettings.instance().session().authenticatedUser()+"\n"+ActionRight.ManageMenus+"\n");
        }
        final MealMenuRepository mealMenuRepository = PersistenceContext.repositories().mealMenu();
        return mealMenuRepository.all();
    }
}
