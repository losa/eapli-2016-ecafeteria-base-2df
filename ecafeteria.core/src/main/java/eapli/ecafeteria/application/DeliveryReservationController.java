/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.cafeteria.WorkTimeCashier;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.util.Console;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

/**
 *
 * @author andreiavilarinho
 */
public class DeliveryReservationController implements Controller {

    /*public void listReservation() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        for (Reservation reserv : PersistenceContext.repositories().reservations().all()) {
            if(reserv!=null){
                System.out.println("ID:" + reserv.codeID() + " CafeteriaUser: " + reserv.user().systemUser().username() + "\n");
            }            
        }
    }*/

    public boolean deliveryReservation() {
        ensurePermissionOfLoggedInUser(ActionRight.Sale);
        //listReservation();
        POS pointOfSale = getPOS();
        if (pointOfSale != null) {
            final Long id = Console.readLong("Reservation id: ");

            Reservation reser = getReservation(id);
            if (reser != null) {
                System.out.println(reser.status());
                reser.deliveryReservation();
                pointOfSale.addRefeicaoEntregue();
                System.out.println("The reservation was registed as delivery.");
                return true;
            }
            System.out.println("The reservation don't exist.");
            return false;
        }

        System.out.println("Point of Sale was not open yet.");
        return false;
    }

    public Reservation getReservation(Long id) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        //Calendar now = Calendar.getInstance();
        //int hourNow = now.get(Calendar.HOUR_OF_DAY);
        //System.out.println("DATE NOW: " + dateFormat.format(Calendar.getInstance().getTime()) + ".\n");
        //System.out.println("HOUR NOW: " + hourNow);
        Iterable<Reservation> listReser = PersistenceContext.repositories().reservations().all();
        Iterator it = listReser.iterator();
        while (it.hasNext()) {
            Reservation reservation = (Reservation) it.next();
            if (reservation.compareID(id)) { //falta comparar com a data de hoje

                Calendar date = reservation.getDate();
                Calendar now = Calendar.getInstance();
                //System.out.println("DATE RESERVATION: " + dateFormat.format(date.getTime()) + ".\n");
                //System.out.println("DATE NOW: " + dateFormat.format(Calendar.getInstance().getTime()) + ".\n");
                WorkTimeCashier wtc;
                WorkTimeCashier wtcNow;
                if (reservation.getDate().get(Calendar.HOUR_OF_DAY) < 15) {
                    wtc = WorkTimeCashier.ALMOCO;
                } else {
                    wtc = WorkTimeCashier.JANTAR;
                }
                if (now.get(Calendar.HOUR_OF_DAY) < 15) {
                    wtcNow = WorkTimeCashier.ALMOCO;
                } else {
                    wtcNow = WorkTimeCashier.JANTAR;
                }
                //if(dateFormat.format(date.getTime()).equals(dateFormat.format(now.getTime()))){
                if (dateFormat.format(now.getTime()).equals(dateFormat.format(date.getTime()))
                        && wtc.equals(wtcNow)) {
                    return reservation;
                }
            }
        }
        return null;
    }

    public POS getPOS() {
        SystemUser user = AppSettings.instance().session().authenticatedUser();
        Iterable<POS> listPos = PersistenceContext.repositories().pointOfSale().all();
        Iterator it = listPos.iterator();
        while (it.hasNext()) {
            POS pointOfSale = (POS) it.next();
            if (pointOfSale.isActive()) {
                if (pointOfSale.id().equals(user)) {
                    return pointOfSale;
                }
            }
        }
        return null;
    }

}
