/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ana
 */
public class RegisterMealInformationController {

    public Iterable<MealPlan> allMealPlans() {
        MealPlanRepository rep = PersistenceContext.repositories().mealPlan();
        return rep.all();
    }

    public Iterable<MealPlanItem> allMenuPlanItemsOfPlan(MealPlan mealPlan) {
        MealPlanItemRepository rep = PersistenceContext.repositories().mealPlanItem();
        Iterable<MealPlanItem> mealPlanItems = rep.all();

        List<MealPlanItem> mealPlanItemsList = new ArrayList(); //check for null

        for (MealPlanItem meal : mealPlanItems) {
            if (meal.mealPlan().descricao().equals(mealPlan.descricao())) {
                mealPlanItemsList.add(meal);
            }
        }

        return mealPlanItemsList;
    }

    public void mealsCooked(int n, MealPlanItem m) {
        //fazer validações
        m.cookedQtt(n);
        MealPlanItemRepository rep = PersistenceContext.repositories().mealPlanItem();
        rep.save(m);
    }

    public void mealsNotSold(int n, MealPlanItem m) {
        //TODO
    }

    /*public Iterable<MealMenu> allMenus() {
        MealMenuRepository rep = PersistenceContext.repositories().mealMenu();
        return rep.all();
    }

    public Iterable<Meal> allMealsOfMenu(MealMenu menu) {
        MealRepository rep = PersistenceContext.repositories().meal();
        Iterable<Meal> meals = rep.all();

        List<Meal> menuMeals = new ArrayList(); //check for null

        for (Meal meal : meals) {
            if (meal.mealMenu().descricao().equals(menu.descricao())) {
                menuMeals.add(meal);
            }
        }

        return menuMeals;
    }

    public MealMenu mealById(long id) {
        MealMenuRepository rep = PersistenceContext.repositories().mealMenu();
        return rep.findById(id);
    }

    public void mealsCooked(int number, Meal m) {
        
        //m.planItem().cookedQtt(number);
        MealRepository rep = PersistenceContext.repositories().meal();
        rep.save(m);
    }

    public void mealsNotSold(int number, MealMenu m) {
        //TODO
    }*/
}
