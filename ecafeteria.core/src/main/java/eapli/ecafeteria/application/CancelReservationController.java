/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.rSortType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservationsRepository;
import eapli.ecafeteria.persistence.jpa.JpaReservationsRepository;
import eapli.framework.application.Controller;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Diogo Vigo
 */
public class CancelReservationController implements Controller {

    ListReservationsController lstReservationsC = new ListReservationsController();
    Reservation reservation = new Reservation();
    ReservationsRepository Rrepository = new JpaReservationsRepository();
    CafeteriaUser user;
    Meal meal;
    Calendar date;
    RepositoryFactory repository = repositories();
    List<Reservation> lstReservation;

    private RepositoryFactory repositories() {
        return PersistenceContext.repositories();
    }

    public boolean StartCancelReservation() {

        AppSettings app = AppSettings.instance();
        SystemUser a = app.session().authenticatedUser();
        this.user = repository.cafeteriaUsers().findBySystemUser(a);
        Scanner scanIn = new Scanner(System.in);
        List<Meal> lstMeal = new ArrayList<>();
        lstReservation = new ArrayList<>();
        for (Reservation res : Rrepository.all()) {
            if (res.user().equals(this.user)) {
                lstMeal.add(res.Meal());
                lstReservation.add(res);
            }
        }
        if (lstMeal.isEmpty()) {
            throw new IllegalArgumentException("There are no Reservations");
        }
        System.out.println("Insert meal to cancel:\n");
        int i = 0;
        for (Meal meal1 : lstMeal) {
            System.out.printf("%d - %s\n", i, meal1.toString());
            i++;
        }
        int ans;
        ans = scanIn.nextInt();
        this.meal = lstMeal.get(ans);
        this.date = lstMeal.get(ans).day();
        cancelReservation(lstReservation.get(ans));
        return true;
    }

    public void setUser(CafeteriaUser User) {
        this.user = User;
    }

    public void meal(Meal meal) {
        this.meal = meal;
    }

    public void dateTime(Calendar date) {
        this.date = date;
    }

    public void cancelReservation(Reservation r) {

        r.cancelReservation();
        boolean cancel = PersistenceContext.repositories().reservations().cancelReservation(r);
        //if (cancel) {
            //this.user.Account().actSaldo(1, date);
        //}

    }

}
