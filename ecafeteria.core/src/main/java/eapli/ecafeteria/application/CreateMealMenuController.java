/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;

/**
 *
 * @author Luís Maia
 */
public class CreateMealMenuController implements Controller {

    public MealMenu CreateMealMenuController(String descricao, Calendar begin, Calendar end) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        final MealMenu newMealMenu = new MealMenu(begin, end, descricao);
        final MealMenuRepository repo = PersistenceContext.repositories().mealMenu();
        
        repo.add(newMealMenu);
        return newMealMenu;

    }

}
