/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;

/**
 *
 * @author Asus
 */
public class LogoutController {
    
    public boolean logout(){        
        AppSettings.instance().removeSession();
        if(AppSettings.instance().session()==null){
            return true;
        }else{
            return false;
        }
    }
    
}
