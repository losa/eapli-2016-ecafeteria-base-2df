/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.framework.application.Controller;

/**
 *
 * @author diogo
 */
public class ListAvailableMenusController implements Controller{
    
    public Iterable<MealMenu> listAvailableMenus() {
        //TODO check if this use case should list all meal menus or only active ones
        return new ListAvailableMenusService().availableMenus();
    }
    
}
