/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.cafeteria.WorkTimeCashier;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.Console;
import java.util.Calendar;
import java.util.Iterator;

public class OpenPOSController implements Controller {

    public POS openPOS() throws DataIntegrityViolationException {
        //CONFIRM IF HAVE PERMISSION
        ensurePermissionOfLoggedInUser(ActionRight.Sale);
        SystemUser user = AppSettings.instance().session().authenticatedUser();
        
        //GET POS 
        POS pointOfSale = getPOS(user);
        if (pointOfSale == null) {
            final Calendar day = Console.readCalendar("Day to work (dd-MM-yyyy): ");
            final WorkTimeCashier wtc;
            int hour = -1;
            do{
                hour = Console.readInteger("Hour to work [0-24]:");
            }while(hour<0 || hour>24);
            if(hour<15){                
                wtc = WorkTimeCashier.ALMOCO;
                //day.set(Calendar.HOUR_OF_DAY, hour);
            }else{
                wtc = WorkTimeCashier.JANTAR;
                //day.set(Calendar.HOUR_OF_DAY, hour);
            }
            pointOfSale = new POS(user, day, wtc);
            PersistenceContext.repositories().pointOfSale().add(pointOfSale);
            System.out.println("Point of Sale Create");
            return pointOfSale;
        }
        if(pointOfSale.isActive()){
            System.out.println("Point of Sale was open yet.");
        }else{
            System.out.println("Point of Sale Open");
            pointOfSale.toActive();        
        }

        return pointOfSale;
    }

    //Get List of Points of Sale Actives
    public POS getPOS(SystemUser user) {
        Iterable<POS> listPos = PersistenceContext.repositories().pointOfSale().all();
        Iterator it = listPos.iterator();
        while (it.hasNext()) {
            POS pointOfSale = (POS) it.next();
            if (pointOfSale.id().equals(user) && pointOfSale.forThisDay()) {
               return pointOfSale;
            }
        }
        return null;
    }

}
