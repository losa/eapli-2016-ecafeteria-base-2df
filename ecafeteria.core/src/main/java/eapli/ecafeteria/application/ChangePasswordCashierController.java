/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.Password;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 *
 * @author RP
 */
public class ChangePasswordCashierController implements Controller {

    private final ListUsersController theController = new ListUsersController();
    //Caso seja 1 dizes q nao tem o tamanho necessario e chamas o metodo outra vez
    //caso seja dois chamas o saveChanges
     public int changePass(String pass) throws UnsupportedEncodingException {

        int fraca = 0, media = 0, forte = 0;
        if (pass.length() < 5) {
            
            return 1;
        }
        byte[] bytes = pass.getBytes("US-ASCII");
        for (int i = 0; i < bytes.length; i++) {
            if ((bytes[i] >= 48 && bytes[i] <= 57) || (bytes[i] >= 97 && bytes[i] <= 122)) {
                fraca++;
            }
            if (bytes[i] >= 65 && bytes[i] <= 90) {
                media++;
            }
            if (bytes[i] == 58 || bytes[i] == 59 || bytes[i] == 63 || bytes[i] == 43 || bytes[i] == 45 || bytes[i] == 95 || bytes[i] == 126) {
                forte++;
            }
        }
        if (fraca != 0 && media != 0 && forte != 0) {
            System.out.println("Password forte");
            return 2;
        }
        if (fraca != 0 && media != 0&& forte == 0) {
            System.out.println("Password media");
            return 2;
        } else{
            System.out.println("Password fraca");
            return 2;
        }
		
	
    }
	
	public boolean saveChanges (String pass, String username){
            final Iterable<SystemUser> iterable = this.theController.listUsers();
            Username u = new Username(username);
            Password p = new Password(pass);
        if (!iterable.iterator().hasNext()) {
            System.out.println("There is no registered User");
        } else {
            for (final SystemUser user : iterable) {
               if(user.username().toString().equals(u.toString())){
                   user.changePassword(p);
                   PersistenceContext.repositories().users().save(user);
                   return true;
               }
            }
            System.out.println("That user dont exists");
        }
        return false;
	}
}
