/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Bolt
 */
public class RegisterItemPlanController implements Controller {

    public MealPlanItem RegisterItemPlanController(MealPlanItem mpi) throws DataIntegrityViolationException {

        ensurePermissionOfLoggedInUser(ActionRight.ManageKitchen);

        PersistenceContext.repositories().mealPlanItem().add(mpi);
        return mpi;

    }

    public Iterable<MealPlan> listPlan() {
        final MealPlanRepository repo = PersistenceContext.repositories().mealPlan();
        //Duvida???ou activeMealMenu() 
        return repo.all();
    }

    public Iterable<Meal> listMeal(Calendar begin) {
        final MealRepository repo = PersistenceContext.repositories().meal();
        return repo.listMeal(begin);

//        
//        List<Meal> lst = new ArrayList<>();
//        List<Meal> aux = new ArrayList<>();
//        final MealRepository repo = PersistenceContext.repositories().meal();
//        
//        lst =(List)repo.all();
//        
//        for (Meal meal: lst) {
//            if (begin.compareTo(meal.day()) == 0) {
//                aux.add(meal);
//            }
//        }
//        
//        return (Iterable) aux;
    }

}
