/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.MealsType;
import eapli.ecafeteria.persistence.MealsTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Bolt
 */
public class ListMealsTypeService {
    
    public Iterable<MealsType> allMealsTypes() {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        final MealsTypeRepository mealsTypeRepository = PersistenceContext.repositories().mealsType();
        return mealsTypeRepository.all();
    }
}
