/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.persistence.*;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author LeandroSousa 1140350 SIGMA
 */
public class MakeReservationsController {

    public MakeReservationsController() {
    }

    private RepositoryFactory repositories() {
        return PersistenceContext.repositories();
    }

    /**
     * Return a menu repository.
     *
     * @return
     * @throws Exception
     */
    private MealMenuRepository menu() throws Exception {
        RepositoryFactory pc = repositories();
        MealMenuRepository menu = pc.mealMenu();
        if (menu == null) {
            throw new IllegalArgumentException("There are no Menus");
        }
        return menu;
    }

    /**
     * returns a list of menus for a given period of time
     *
     * @param start
     * @param end
     * @return
     * @throws Exception
     */
    public MealMenu listMenu(Calendar start, Calendar end) throws Exception {
        MealMenuRepository m = menu();
        List<MealMenu> listMenu = m.listMealMenu(start, end);
        if (listMenu == null || listMenu.isEmpty()) {
            throw new IllegalArgumentException("There are no MealMenu");
        }
        return listMenu.get(0);
    }

    /**
     *
     * @param day
     * @return
     */
    public Iterable<Meal> chooseMeal(Calendar day) {
        RepositoryFactory pc = repositories();
        MealRepository repositorio = pc.meal();
        Iterable<Meal> m_meal = repositorio.listMeal(day);
        if (m_meal == null) {
            throw new IllegalArgumentException("There are no Meals");
        }
        return m_meal;
    }

    /**
     * Create and save a new reserva.
     *
     * @param m
     * @throws eapli.framework.persistence.DataIntegrityViolationException
     */
    public void newReserva(Meal m) throws DataIntegrityViolationException {
        try {
            RepositoryFactory repository = repositories();
            AppSettings app = AppSettings.instance();
            SystemUser a = app.session().authenticatedUser();
            CafeteriaUser user = repository.cafeteriaUsers().findBySystemUser(a);
            if (user == null) {
                throw new IllegalArgumentException("Não associou um user.");
            }
            Reservation r = Reservation.newReservation(m, DateTime.now(), user);

            //Movimento mov = new Movimento();
            //mov.movCharge(user.Account(), -1);//Default value of a cost of meal
            repository.reservations().add(r);
            System.out.printf("%s\n", "Reserva efectuada com sucesso.");

        } catch (IllegalArgumentException a) {
            System.out.println(a.getMessage());
        }
    }
}
