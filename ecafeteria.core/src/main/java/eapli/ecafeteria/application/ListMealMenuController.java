/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

/**
 *
 * @author Bolt
 */
public class ListMealMenuController implements Controller {
    
    public Iterable<MealMenu> listMenus(){
        final MealMenuRepository repo = PersistenceContext.repositories().mealMenu();
        //Duvida???ou activeMealMenu() 
        return repo.all();
    }
    
    public Iterable<Meal> listMeals(){
        final MealRepository repo = PersistenceContext.repositories().meal();
        return repo.all();
    }
}
