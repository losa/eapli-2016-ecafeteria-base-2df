/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.mealbooking.ReservationComparator;
import static eapli.ecafeteria.domain.mealbooking.ReservationComparator.getComparator;
import eapli.ecafeteria.domain.mealbooking.rSortType;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.ReservationsRepository;
import eapli.framework.application.Controller;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Bruno
 */
public class ListReservationsController implements Controller {

    public final int FUTURE_RESERVATIONS_OPTION = 0;
    public final int PREVIOUS_RESERVATIONS_OPTION = 1;

    public ListReservationsController() {
    }

    public List<Reservation> listReservations(rSortType sort_type) {
        //ISTO ESTA A DIZER QUE NAO TENHO PERMISSOES ENQUANTO O KM TA A CORRER A APP!?!!?!?
        
        ensurePermissionOfLoggedInUser(ActionRight.ManageKitchen);
        //ensurePermissionOfLoggedInUser(ActionRight.Sale);

        final ReservationsRepository reservationsRepository = PersistenceContext.repositories().reservations();
        Iterable<Reservation> it = reservationsRepository.all();
        List<Reservation> list = new ArrayList<>();

        if (!it.iterator().hasNext()) {
            System.out.println("There are no reservations...");
        } else {

            for (final Reservation res : it) {
                list.add(res);
            }
            switch (sort_type) {
                case DAY:
                    Collections.sort(list, getComparator(ReservationComparator.DAY_SORT));
                    break;
                case TYPE:
                    Collections.sort(list, getComparator(ReservationComparator.TYPE_SORT));
                    break;
                case DISH:
                    Collections.sort(list, getComparator(ReservationComparator.DISH_SORT));
                    break;
                case MEAL:
                    Collections.sort(list, getComparator(ReservationComparator.MEAL_SORT));
                    break;
            }
        }
        return list;
    }

    public Iterable<Reservation> getFutureReservations(int days) {
        return new ListReservationsService().listFutureReservations(days);
    }

    public Iterable<Reservation> getPreviousReservations() {
        return new ListReservationsService().listPreviousReservations();
    }

}
