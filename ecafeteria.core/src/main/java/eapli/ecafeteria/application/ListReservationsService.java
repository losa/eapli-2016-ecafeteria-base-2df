/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.persistence.ReservationsRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author SIGMA
 */
public class ListReservationsService {
    public ListReservationsService(){}
    
    public Iterable<Reservation> listFutureReservations(int days){
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        final ReservationsRepository repo = PersistenceContext.repositories().reservations();
        return repo.futureReservations(days);
    }
    
    public Iterable<Reservation> listPreviousReservations(){
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        final ReservationsRepository repo = PersistenceContext.repositories().reservations();
        return repo.previousReservations();
    }
    
    public Iterable<Reservation> listRatableReservations(){
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        final ReservationsRepository repo = PersistenceContext.repositories().reservations();
        return repo.previousReservations();
    }
}
