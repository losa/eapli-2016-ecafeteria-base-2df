/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.domain.meals.MealsType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealsTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;

/**
 *
 * @author Luís Maia
 */
public class RegisterMealsController implements Controller {
    
    public Meal RegisterMealsController (Dish dish, MealsType mealsType, Calendar day, MealMenu menu) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        
        final Meal newMeal = new Meal(dish, mealsType, day, menu);
        final MealRepository repo = PersistenceContext.repositories().meal();
        
        repo.add(newMeal);
        return newMeal;
    }
    
    
    public Iterable<MealMenu> listMenus(){
        final MealMenuRepository repo = PersistenceContext.repositories().mealMenu();
        //Duvida???ou activeMealMenu() 
        return repo.all();
    }
    
    
    public Iterable<MealsType> listMealsTypes(){
        final MealsTypeRepository repo = PersistenceContext.repositories().mealsType();
        return repo.all();
    }
    
    public Iterable<Dish> listDish(){
        final DishRepository repo = PersistenceContext.repositories().dish();
        return repo.all();
    }
}
