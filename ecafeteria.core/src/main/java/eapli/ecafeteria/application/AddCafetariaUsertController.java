/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.mealbooking.Saldo;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Asus
 */
public class AddCafetariaUsertController {
    
    public CafeteriaUser addCafeteriaUser(SystemUser user) throws DataIntegrityViolationException{
        
        Iterable<CafeteriaUser> cafUser = PersistenceContext.repositories().cafeteriaUsers().all();
        int mecNumber = 1;
        for (CafeteriaUser cafeteriaUser : cafUser) {
            mecNumber++;
        }        
        String str = String.valueOf(mecNumber);
        Account account = createAccount(str);
        CafeteriaUser cu = new CafeteriaUser(user, PersistenceContext.repositories().organicUnits().all().iterator().next(), new MecanographicNumber(str), account);
        
        PersistenceContext.repositories().cafeteriaUsers().add(cu);
        return null;
    }    
    
    public Account createAccount(String id) throws DataIntegrityViolationException{        
        Saldo saldo = new Saldo();
        PersistenceContext.repositories().balance().add(saldo);
        Account account = new Account(id,saldo);
        PersistenceContext.repositories().account().add(account);
        return account;
        
    }
}
