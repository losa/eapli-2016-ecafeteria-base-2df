/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.MealsType;
import eapli.ecafeteria.persistence.MealsTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Bolt
 */
public class RegisterTypeMealsController implements Controller{
 
    public MealsType registerMealsType(String acronym, String description) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        final MealsType newDishType = new MealsType(acronym, description);
        final MealsTypeRepository repo = PersistenceContext.repositories().mealsType();
        // FIXME error checking if the newDishType is already in the persistence
        // store
        repo.add(newDishType);
        return newDishType;
    }
}
