/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.application.ListDishService;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.application.Controller;

/**
 *
 * @author Luís Maia
 */
public class ListDishController implements Controller {

    public Iterable<Dish> listDish() {
        //TODO check if this use case should list all dish or only active ones
        return new ListDishService().allDish();
    }

}
