/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.mealbooking.Movimento;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Iterator;
import java.util.Objects;

/**
 *
 * @author RP
 */
public class AccountChargeController implements Controller {

    public Account getAccountID(MecanographicNumber number) {
        CafeteriaUser cafUser = findCafUserByNumber(number);
        
        if(cafUser!=null){
            return cafUser.Account();
        }
        return null;
    }

    public boolean chargeAccount(double saldo, Account account) throws DataIntegrityViolationException {
        Movimento m = new Movimento();
        if(m.movCharge(account, saldo) == true){
            if(m.saveMov(m) == true){
                return true;
            }
        }
        return false;
    }
    
    public CafeteriaUser findCafUserByNumber(MecanographicNumber number){
        Iterable<CafeteriaUser> cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers().all();
        Iterator it = cafeteriaUserRepository.iterator();   
        while(it.hasNext()){
            CafeteriaUser user = (CafeteriaUser) it.next();
            if(user.is(number)){
                System.out.println("USER: " + user.Name() + " UserNumber: " + user.id().toString() + "\nSaldo:" + user.Account().saldo() + "\n");
            }

            if(user.id().toString().equals(number.toString())){
                return user;
            }
        }
        return null;
    }
}
