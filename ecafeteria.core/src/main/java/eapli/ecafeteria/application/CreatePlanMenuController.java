/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;

/**
 *
 * @author Bolt
 */
public class CreatePlanMenuController implements Controller{
    
    public MealPlan CreateMealPlanController(String descricao, Calendar begin, Calendar end) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageKitchen);

        final MealPlan newMealPlan = new MealPlan(begin, end, descricao);
        final MealPlanRepository repo = PersistenceContext.repositories().mealPlan();
        
        repo.add(newMealPlan);
        return newMealPlan;

    }
    
    public MealPlan CreateMealPlanController(MealPlan newMealPlan) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageKitchen);
        final MealPlanRepository repo = PersistenceContext.repositories().mealPlan();
        
        repo.add(newMealPlan);
        return newMealPlan;

    }
}
