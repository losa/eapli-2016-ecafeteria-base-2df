/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;

/**
 *
 * @author Sofia
 */
public class ChangeOrganicUnitController implements Controller{

    public Iterable<OrganicUnit> listOrganicUnits(){
        ensurePermissionOfLoggedInUser(ActionRight.Administer);
        ArrayList<OrganicUnit> activeOrganicUnits = new ArrayList<>();
        
        final OrganicUnitRepository organicUnitRepository = PersistenceContext.repositories().organicUnits();
        for (OrganicUnit organicUnit : organicUnitRepository.all()) {
            if(organicUnit.isActive()){
                activeOrganicUnits.add(organicUnit);
            }
        }

        return activeOrganicUnits;
    }

    public OrganicUnit changeOrganicUnit(OrganicUnit organicUnit, String name, String description) throws DataIntegrityViolationException{
        ensurePermissionOfLoggedInUser(ActionRight.Administer);
        
        organicUnit.changeNameAndDescription(name, description);
        
        final OrganicUnitRepository organicUnitRepository = PersistenceContext.repositories().organicUnits();
        organicUnit = organicUnitRepository.save(organicUnit);
        return organicUnit;
    }
    
}
