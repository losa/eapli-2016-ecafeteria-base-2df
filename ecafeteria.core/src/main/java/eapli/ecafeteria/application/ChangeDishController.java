/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

/**
 *
 * @author Luís Maia
 */
public class ChangeDishController implements Controller {

    public Dish changeDishName(Dish theDish, String newDishName) {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        if (theDish == null) {
            throw new IllegalStateException();
        }

        theDish.changeNameTo(newDishName);

        final DishRepository repo = PersistenceContext.repositories().dish();
        return repo.save(theDish);
    }
    
        /**
     * in the context of this use case only active dish types are meaningful.
     *
     * @return
     */
    public Iterable<Dish> listDish() {
        return new ListDishService().activeDish();
    }
}
