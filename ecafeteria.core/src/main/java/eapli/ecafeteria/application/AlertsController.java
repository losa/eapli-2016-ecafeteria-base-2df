/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafetaria.persistence.obs.WarningsBoard;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Ardyvee
 */
public class AlertsController {
    
    public Iterable<String> allAlertsFrom(Calendar day) {
        List<String> alertas = new ArrayList();
        WarningsBoard wb = new WarningsBoard();
        MealPlanRepository mpr = PersistenceContext.repositories().mealPlan();
        Iterable<MealPlan> mp_it = mpr.all();
        if(mp_it == null) {
            return null;
        }
        List<MealPlan> list_mp = new ArrayList();
        for(MealPlan mp : mp_it) {
            if(day.before(mp.period().finalDay())) {
                list_mp.add(mp);
            }
        }
        MealPlanItemRepository mpir = PersistenceContext.repositories().mealPlanItem();
        Iterable<MealPlanItem>mpir_it = mpir.all();
        for(MealPlanItem mpi : mpir_it) {
            for(MealPlan mp2 : list_mp) {
                if(mp2.id().equals(mpi.mealPlan().id())) {
                    String alerta = wb.updateStatus(mpi);
                    if(alerta != null) {
                        alertas.add(alerta);
                    }
                }
            }
        }
        
        return alertas.isEmpty() ? null : alertas;
    }
}
