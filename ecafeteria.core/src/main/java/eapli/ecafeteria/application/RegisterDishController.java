/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Luís Maia
 */
public class RegisterDishController implements Controller {

    public Dish RegisterDishController(String name, Double price, Double calorias, Double sal, Double gordura, DishType dishType) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);
        final Dish newDish = new Dish(name, price, calorias, sal, gordura, dishType);
        final DishRepository repo = PersistenceContext.repositories().dish();
        // FIXME error checking if the newDish is already in the persistence
        // store
        repo.add(newDish);
        return newDish;
    }
    public Iterable<DishType> listDishTypesAvaiable(){
        final DishTypeRepository repo = PersistenceContext.repositories().dishTypes();
        return repo.activeDishTypes();
    }
}
