/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.mealbooking.ReservationComparator;
import static eapli.ecafeteria.domain.mealbooking.ReservationComparator.getComparator;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Rating;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nuno Barbosa 1140372 SIGMA
 */
public class RateMealController implements Controller {

    private final RepositoryFactory repository = PersistenceContext.repositories();
    private Meal mealToRate;
    private Rating rating;

    public RateMealController() {
        this.mealToRate = null;
        this.rating = new Rating();
    }

    public Iterable<Reservation> getCompleteReservationList() {
//        ensurePermissionOfLoggedInUser(ActionRight.);
        Iterable<Reservation> it = getPreviousReservations();
        List<Reservation> list = new ArrayList<>();
        AppSettings app = AppSettings.instance();
        SystemUser a = app.session().authenticatedUser();
        CafeteriaUser user = repository.cafeteriaUsers().findBySystemUser(a);

        if (!it.iterator().hasNext()) {
            System.out.println("There are no reservations...");
        } else {

            for (final Reservation res : it) {
                if (res.status() == Reservation.enumStatus.DELIVERY) {
                    list.add(res);
                }
            }
            Collections.sort(list, getComparator(ReservationComparator.DAY_SORT));

        }
        ArrayList<Reservation> lista = (ArrayList<Reservation>) list;
        Iterable<Reservation> iterReservation = list;
        return iterReservation;
    }

    public void selectMeal(Reservation res){
        this.rating.addMeal(res.Meal());
    }

    public boolean rateMeal(int rating){
        return this.rating.rate(rating);
    }
    
    public void saveRating(){
        try {
            repository.ratings().add(this.rating);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(RateMealController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private Iterable<Reservation> getPreviousReservations() {
        return new ListReservationsService().listPreviousReservations();
    }

}
