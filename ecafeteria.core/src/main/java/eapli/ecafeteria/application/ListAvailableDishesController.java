/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 1120250
 */
public class ListAvailableDishesController implements Controller {

    public Iterable<MealPlanItem> allMealsPlanItem() {
        ensurePermissionOfLoggedInUser(ActionRight.Sale);
        final MealPlanItemRepository mpi = PersistenceContext.repositories().mealPlanItem();
        return mpi.all();
    }

    public Map<Integer, MealPlanItem> availableDishes() {

        Map<Integer, MealPlanItem> mapDish = new HashMap<>();

        Iterable<MealPlanItem> mpi = allMealsPlanItem();

        final DishTypeRepository dtr = PersistenceContext.repositories().dishTypes();
        final DishRepository dr = PersistenceContext.repositories().dish();
        
        int contador = 1;
        for (MealPlanItem mi : mpi) {

            mapDish.put(contador, mi);
            contador++;
        }

        return mapDish;
    }

}
