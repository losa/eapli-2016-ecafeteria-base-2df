/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import eapli.framework.domain.AggregateRoot;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.Strings;
import java.util.Calendar;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author arocha
 */
@Entity
public class OrganicUnit implements AggregateRoot<String>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String acronym;
    private String name;
    private String description;
    @Temporal(TemporalType.DATE)
    private Calendar createdOn;
    private boolean active;
    @Temporal(TemporalType.DATE)
    private Calendar deactivatedOn;

    protected OrganicUnit() {
        // for ORM
    }

    public OrganicUnit(String acronym, String name, String description) {
        if (acronym == null || name == null || description == null || acronym.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
        // TODO name and description provably should not be empty
        this.acronym = acronym;
        this.name = name;
        this.description = description;
        this.active = true;
    }

    @Override
    public String id() {
        return this.acronym;
    }

    @Override
    public boolean is(String id) {
        return id.equalsIgnoreCase(this.acronym);
    }

    public boolean isActive() {
        return this.active;
    }

    public String name() {
        return this.name;
    }

    public String description() {
        return this.description;
    }

    // FIXME implement equals() and hashCode()

    @Override
    public boolean sameAs(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    public void deactivate(Calendar deactivatedOn) {
        if (!active) {
            System.out.println("");
        } else if (deactivatedOn.before(this.createdOn)) {
            System.out.println("");
        } else if (deactivatedOn == null) {
            throw new IllegalArgumentException();
        } else {
            this.active = false;
            this.deactivatedOn = deactivatedOn;
        }
    }

    public void changeNameAndDescription(String name, String description) throws DataIntegrityViolationException {
        if (Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(description)) {
            throw new DataIntegrityViolationException();
        } else {
            this.name = name;
            this.description = description;
        }
    }

    @Override
    public int hashCode() {
        return this.acronym.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrganicUnit other = (OrganicUnit) obj;

        return this.acronym.equals(other.acronym);
    }
}
