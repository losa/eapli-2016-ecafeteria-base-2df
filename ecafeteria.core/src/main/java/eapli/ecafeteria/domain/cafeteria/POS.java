package eapli.ecafeteria.domain.cafeteria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.framework.domain.AggregateRoot;
import eapli.util.DateTime;
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

/**
 *
 * @author 1130107
 */

@Entity
public class POS implements AggregateRoot<SystemUser>, Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @JoinColumn
    private SystemUser user;
    private Calendar day;
    private WorkTimeCashier hour;
    private static boolean active;
    private static int refeicoesEntregues;
    private static int vendasUltimaHora;
    
    //@ManyToOne
    //private OrganicUnit organicUnit;

    protected POS() {
        // for ORM
    }
    
    public POS(SystemUser user, Calendar day, WorkTimeCashier hour){
        if (user == null || day == null || hour == null) {
            throw new IllegalArgumentException();
        }
        this.user = user;
        this.day = day;
        this.hour = hour;
        this.active = false;
        //this.organicUnit = orgUnit;
    }
    
    public int getRefeicoesEntregues(){
        return this.refeicoesEntregues;
    }
    
    public void addRefeicaoEntregue(){
        this.refeicoesEntregues++;
    }
    
    public int getVendasUltimaHora(){
        return this.vendasUltimaHora;
    }
    
    public void addVendasUltimaHora(){
        this.vendasUltimaHora++;
    }
    
    public void toActive(){
        this.active=true;
        //System.out.println("FICOU TRUE");
    }
    
    public void toInactive(){
        this.active=false;
    }
    
    public boolean isActive() {
        return this.active;
    }
    
    public Calendar getDay(){
        return this.day;
    }
    
    public boolean forThisDay(){
        int dia = this.day.get(Calendar.DAY_OF_MONTH);
        int dia2 = DateTime.now().get(Calendar.DAY_OF_MONTH);
        int mes = this.day.get(Calendar.MONTH);
        int mes2 = DateTime.now().get(Calendar.MONTH);
        int ano = this.day.get(Calendar.YEAR);
        int ano2 = DateTime.now().get(Calendar.YEAR);
        if(dia == dia2 && mes == mes2 && ano == ano2){
            WorkTimeCashier wtc;
            //System.out.println("HORA: " + this.day.get(Calendar.HOUR));
            Calendar calendar = GregorianCalendar.getInstance();
            if(calendar.get(Calendar.HOUR_OF_DAY)<15){
                wtc = WorkTimeCashier.ALMOCO;
            }else{
                wtc = WorkTimeCashier.JANTAR;
            }
            if(wtc.equals(this.hour)){
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean sameAs(Object other) {
         if (!(other instanceof POS)) {
            return false;
        }

        final POS that = (POS) other;
        if (this == that) {
            return true;
        }
        
        if (!this.user.sameAs(other)) {
            return false;
        }
        
        return true;
    }

    @Override
    public boolean is(SystemUser id) {
        return id.equals(this.user);
    }

    @Override
    public SystemUser id() {
        return this.user;
    }

    public WorkTimeCashier Hour() {
        return this.hour;
    }
    
    @Override
    public String toString(){
        String wtc = "none";
         if(this.hour == WorkTimeCashier.ALMOCO){
             wtc = "almoço" ;
         }else{
             wtc = "jantar" ;
         }
         return "User " + this.user.id() + "Date: " + this.day + "Horario: " + wtc + "\n";
    }
          
}
