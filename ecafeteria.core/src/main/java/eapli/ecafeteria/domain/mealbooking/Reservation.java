/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.domain.meals.Meal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.*;

/**
 *
 * @author LeandroSousa 1140350 SIGMA
 */
@Entity
public class Reservation {

    @Id
    @GeneratedValue
    private Long id;
    private Meal meal;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar date;
    private CafeteriaUser user;
    private static enumStatus status;

    public enum enumStatus {
        NOT_DELIVERY(1), DELIVERY(2), REJECTED(3), WAITING(0);
        
         private int status;

        enumStatus(int status) {
            this.status = status;
        }

        public int getStatus() {
           return status;
        }
    }

    /**
     * Complete constructor for a reservation.
     *
     * @param meal
     * @param date
     * @param user
     */
    public Reservation(Meal meal, Calendar date, CafeteriaUser user) {

        this.date = date;
        Reservation.status = enumStatus.WAITING;
        this.meal = meal;
        this.user = user;
    }

    /**
     * Empty constructor to use with the ORM
     */
    public Reservation() {
        //for ORM
    }

    /**
     * return a meal of a reservation
     */
    public Meal Meal() {
        return meal;
    }

    public CafeteriaUser user() {
        return this.user;
    }

    /**
     * return a atual status of reservation
     *
     */
    public enumStatus status() {
        return this.status;
    }
    
    public Long codeID(){
        return this.id;
    }

    /**
     * reject a reservation
     */
    public void rejectReservation() {
        this.status = enumStatus.REJECTED;
    }

    /**
     * Delivery a reservation.
     */
    public void deliveryReservation() {
        this.status = enumStatus.DELIVERY;
    }

    public boolean compareID(Long id) {
        return this.id.equals(id);
    }

    /**
     * Cancel a reservation.
     */
    public void cancelReservation() {
        this.status = enumStatus.NOT_DELIVERY;
    }

    /**
     * return a date of reservation
     *
     * @return 
     */
    public int Date() {
        return date.DATE;
    }
    
    public Calendar getDate(){
        return this.date;
    }

    /**
     * return a new reservation
     *
     * @param meal
     * @param date
     * @param user
     * @return
     */
    public static Reservation newReservation(Meal meal, Calendar date, CafeteriaUser user) {
        return new Reservation(meal, date, user);
    }
    
    @Override
    public String toString(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
         
        return  "ID: "+this.id+"; Data: "+df.format(date.getTime()) +" -> Meal Type: "+this.meal.mealType().description()+" | Dish: "+
                this.meal.dish().name()+"-Dish Type: "+this.meal.dish().dishType();
    }


}
