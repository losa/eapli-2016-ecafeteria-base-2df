/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.persistence.PersistenceContext;
import java.io.Serializable;
import eapli.util.Strings;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
// FIXME Account is almost certainly an entity (part of the CafeteriaUser
// aggregate) and not a value object
@Entity
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;
    
    @Column(unique = true)
    private String accID;
    private Saldo m_saldo ;
    
    public Account(String account, Saldo saldo) {
        if (Strings.isNullOrEmpty(account)) {
            throw new IllegalStateException("Account should neither be null nor empty");
        }
        // FIXME validate invariants, i.e., account regular expression
        this.accID = account;
        this.m_saldo = saldo;
    }

    protected Account() {
        // for ORM
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }

        final Account that = (Account) o;

        return this.accID.equals(that.accID);
    }
    
    public double saldo(){
        return this.m_saldo.saldo();
    }
    
    public boolean actSaldo(double valor,Calendar dom) {          
        if(m_saldo.validaSaldo(valor)) {            
            m_saldo.actualizaSaldo(valor,dom);
            PersistenceContext.repositories().balance().save(this.m_saldo);
            return true;
        }    
        return false;
    }
     
    @Override
    public int hashCode() {
        return this.accID.hashCode();
    }

    @Override
    public String toString() {
        return this.accID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}