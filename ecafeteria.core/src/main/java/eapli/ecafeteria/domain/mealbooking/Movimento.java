/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.persistence.MovimentoRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author 1120250
 */

@Entity
public class Movimento implements Serializable {
    //TODO yet
    
    @Id
    @GeneratedValue
    private Long id;
    private Account acc;
    private double cred;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar day;
    
    
    public Movimento() {
    
    }
    
    
    public Movimento(Calendar dayOfMovement) {
        this.day = dayOfMovement;
    }
    
    public boolean movCharge( Account account, double creditos) {
        this.acc = account;
        this.cred = creditos;
        return acc.actSaldo(cred,day);
    
    }
    
    public Account getAcc() {
        return this.acc;
    }

    public void setAcc(Account acc) {
        this.acc = acc;
    }
    
    public boolean saveMov(Movimento m) throws DataIntegrityViolationException{
        return PersistenceContext.repositories().movimento().add(m);
    }
}
