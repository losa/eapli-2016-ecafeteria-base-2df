/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import java.util.Calendar;
import java.util.Comparator;

/**
 *
 * @author Bruno
 */
public enum ReservationComparator implements Comparator<Reservation> {
    DAY_SORT {
        public int compare(Reservation r1, Reservation r2) {
            return r1.getDate().get(Calendar.DAY_OF_MONTH)-r2.getDate().get(Calendar.DAY_OF_MONTH);
            
        }},
    TYPE_SORT {
        public int compare(Reservation r1, Reservation r2) {
            return r1.status().compareTo(r2.status());
        }},
    DISH_SORT{
         public int compare(Reservation r1, Reservation r2) {
             return r1.Meal().dish().dishType().compareTo(r2.Meal().dish().dishType());
         }
    },
    MEAL_SORT{
        public int compare(Reservation r1, Reservation r2) {
             return 0;
         }
    };

    /*public static Comparator<Reservation> decending(final Comparator<Reservation> other) {
        return new Comparator<Reservation>() {
            public int compare(Reservation o1, Reservation o2) {
                return -1 * other.compare(o1, o2);
            }
        };
    }
*/
    public static Comparator<Reservation> getComparator(ReservationComparator option) {
        return new Comparator<Reservation>() {
            public int compare(Reservation r1, Reservation r2) {
                    int result = option.compare(r1, r2);
                    return result;
            }
        };
    }
}
