/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author 1120250
 */
@Entity
public class Saldo implements Serializable{
    
    @Id    
    @GeneratedValue
    private Long id;
    private double valor;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar day;
    
    public Saldo() {        
        this.valor = 0.0;        
    }
        
    
    public void actualizaSaldo(double valor,Calendar dayOfMovement) {
        this.day = dayOfMovement;
        this.valor += valor;        
    }
    
    public boolean validaSaldo(double preco) {        
       if(this.valor >= preco) {           
           return true;           
       }       
       else return false;        
    }
    
        
    public Calendar day() {
        return this.day;
    }
    
    public double saldo(){
        return this.valor;
    }
    
    public boolean validaCarregamento(double v) {        
        if( v > 0.0) {        
            return true;           
        }        
        else return false;
    }
}
