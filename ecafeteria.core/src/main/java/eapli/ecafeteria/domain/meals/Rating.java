/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Nuno Barbosa 1140372 SIGMA
 */
@Entity
public class Rating {
    
    @Id
    @GeneratedValue
    private long id;
    
    private int rating;
    private Meal meal;
    
    public Rating(){
    }
    
    public Rating(int rating, Meal meal) {
        this.rating = rating;
        this.meal = meal;
    }
    
    public String id() {
        return ""+this.id;
    }

    public boolean addMeal(Meal m){
        if(this.meal != null){
            this.meal = m;
            return true;
        }else{
            return false;
        }
    }
    public boolean validateRating(int rating){
        if(rating>0 && rating<=5){
            return true;
        }
        return false;
    }
    
    public boolean rate(int rating){
        if(validateRating(rating)){
            this.rating = rating;
            return true;
        }
        System.out.println("Não consigo dar um rating a refeição...");
        return false;
    }
    
    public String ratingDescription(){
        return "Dia:"+this.meal.day().toString()+" Prato:"+this.meal.dish().name()+" Rating: "+this.rating;
    }
}
