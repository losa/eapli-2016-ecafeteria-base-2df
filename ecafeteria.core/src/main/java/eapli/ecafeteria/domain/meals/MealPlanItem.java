/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Luís Maia
 */
@Entity
public class MealPlanItem implements AggregateRoot<Meal>, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private Meal meal;
    
    private MealPlan mealPlan;
    
    private int intendedQtt;
    private int avaiableQtt;
    private int cookedQtt;
    private int delieredQtt;
    private int undeliveredQtt;

    public MealPlanItem() {
    }

    public MealPlanItem(Meal meal, MealPlan mealPlan, int intendedQtt) {
        this.meal = meal;
        this.mealPlan = mealPlan;
        this.intendedQtt = intendedQtt;
    }

    public MealPlanItem(Meal meal, MealPlan mealPlan) {
        this.meal = meal;
        this.mealPlan = mealPlan;
    }
    
    public MealPlanItem(MealPlanItem otherMealPlanItem) {
        this.avaiableQtt = otherMealPlanItem.avaiableQtt();
        this.cookedQtt = otherMealPlanItem.cookedQtt();
        this.delieredQtt = otherMealPlanItem.delieredQtt();
        this.undeliveredQtt = otherMealPlanItem.undeliveredQtt();
        this.meal = otherMealPlanItem.meal();
    }

    public void cookedQtt(int nr) {
        this.cookedQtt = nr;
    }

    public void avaiableQtt(int nr) {
        this.avaiableQtt = nr;
    }

    public void delieredQtt(int nr) {
        this.delieredQtt = nr;
    }

    public void intendedQtt(int nr) {
        this.intendedQtt = nr;
    }

    public int intendedQtt() {
        return this.intendedQtt;
    }
    public void undeliveredQtt(int nr) {
        this.undeliveredQtt = nr;
    }

    public int cookedQtt() {
        return this.cookedQtt;
    }

    public int avaiableQtt() {
        return this.avaiableQtt;
    }

    public int delieredQtt() {
        return this.delieredQtt;
    }

    public int undeliveredQtt() {
        return this.undeliveredQtt;
    }

    public int decAvaiableQtt() {
        return this.avaiableQtt--;
    }
    
    public Meal meal() {
        return this.meal;
    }
    
    public void meal(Meal meal){
        this.meal = meal;
    }

    public MealPlan mealPlan() {
        return mealPlan;
    }

    public void mealPlan(MealPlan mealPlan) {
        this.mealPlan = mealPlan;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public boolean is(Meal id) {
        return id.equals(meal);
    }

    @Override
    public Meal id() {
        return meal();
    }

}
