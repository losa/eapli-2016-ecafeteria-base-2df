/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.util.Strings;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;

/**
 *
 * @author Luís Maia
 */
@Embeddable
class DishName implements Serializable {

    private static final long serialVersionUID = 1L;
    private String dishName;

    public DishName(String dishName) {
        if (Strings.isNullOrEmpty(dishName)) {
            throw new IllegalStateException("username should neither be null nor empty");
        }
        this.dishName = dishName;
    }
    
    public DishName(){}

    @Override
    public int hashCode() {
        return this.dishName.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DishName)) {
            return false;
        }

        final DishName other = (DishName) o;

        return this.dishName.equals(other.dishName);

    }

    @Override
    public String toString() {
        return this.dishName;
    }

}
