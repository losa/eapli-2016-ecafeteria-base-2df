/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import eapli.util.Strings;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author Luís Maia
 */
@Entity
public class Dish implements AggregateRoot<String>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private DishName name;
    private Double price;
    private NutritionalInfo nutInfo;
    private boolean active;
    @OneToOne
    private DishType dishType;

    protected Dish() {
        // for ORM
    }

    public Dish(Dish otherDish) {
        this.name = otherDish.name;
        this.price = otherDish.price;
        this.nutInfo = otherDish.nutInfo;
        this.active = true;
        this.dishType = otherDish.dishType;
    }

    public Dish(String name, Double price, Double calorias, Double sal, Double gordura, DishType dishType) {
        if (Strings.isNullOrEmpty(dishType.description()) || Strings.isNullOrEmpty(name) || Double.isNaN(price)) {
            throw new IllegalArgumentException("Values should neither be null nor empty");
        }
        this.name = new DishName(name);
        this.price = price;
        this.nutInfo = new NutritionalInfo(calorias, sal, gordura);
        this.active = true;
        this.dishType = dishType;/*DishTypeSet()?*/

    }

    public String name() {
        return this.name.toString();
    }

    public String price() {
        return this.price.toString();
    }

    public String dishType() {
        return this.dishType.id();
    }

    public String nutInfo() {
        return this.nutInfo.toString();
    }

    public boolean isActive() {
        return this.active;
    }

    public void addDishType(DishType dishType) {
        this.dishType = dishType;
    }

    public void changeDishState() {
        this.active = !this.active;
    }

    public void changeNameTo(String newName) {
        if (Strings.isNullOrEmpty(newName)) {
            throw new IllegalArgumentException("Name given is not valid.");
        }
        this.name = new DishName(newName);
    }

    @Override
    public boolean sameAs(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean is(String id) {
        return id.equalsIgnoreCase(name());
    }

    @Override
    public String id() {
        return this.name();
    }
}
