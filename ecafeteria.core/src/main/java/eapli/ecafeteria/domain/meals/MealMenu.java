/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import eapli.framework.domain.TimePeriod2;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Luís Maia
 */
@Entity
public class MealMenu implements AggregateRoot<String>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private String descricao;
    private boolean published;
    
    @Embedded
    private TimePeriod2 period;

    public MealMenu() {
        published = true;
    }

    public MealMenu(Calendar begin, Calendar end, String descricao) {
        this.period = new TimePeriod2(begin, end);
        this.descricao = descricao;
        published = true;
    }

    public String descricao() {
        return this.descricao;
    }

    public TimePeriod2 period() {
        return this.period;
    }
    
    @Override
    public boolean sameAs(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isActive() {
        return this.published;
    }

    @Override
    public boolean is(String id) {
       return id.equals(descricao);
    }

    @Override
    public String id() {
        return this.descricao();
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @param begin
     * @param end
     */
    public void setPeriod(Calendar begin, Calendar end) {
        this.period = new TimePeriod2(begin, end);
    }
}
