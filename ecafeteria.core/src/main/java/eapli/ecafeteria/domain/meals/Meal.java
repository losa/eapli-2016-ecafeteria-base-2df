/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Nuno Barbosa 1140372 SIGMA
 * @author Luís Maia TETA's team
 */
@Entity
public class Meal implements AggregateRoot<MealMenu>, Serializable{

    private static final long serialVersionUID = 1L;
   
    @Id
    @GeneratedValue
    private Long id;
    
    
    private Dish dish;
    private MealsType mealType;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar day;
    
    private MealMenu mealMenu;

    /**
     * Empty constructor to use with the ORM
     */
    public Meal() {

    }

    /**
     * Constructor that copies the content of another instance of a meal
     *
     * @param otherMeal the instance to copy from
     */
    public Meal(Meal otherMeal) {
        this.day = otherMeal.day();
        this.mealMenu = otherMeal.mealMenu();
        this.mealType = otherMeal.mealType();
    }

    /**
     * Complete constructor for a meal.
     *
     * @param dish
     * @param mealType Type of the meal
     * @param date
     * @param mealMenu
     */
    public Meal(Dish dish, MealsType mealType, Calendar date, MealMenu mealMenu) {
        if (dish == null || mealType == null) {
            throw new IllegalArgumentException("Values should neither be null nor empty");
        }
        this.dish = dish;
        this.mealType = mealType;
        this.day = date;
        this.mealMenu = mealMenu;     
    }

    /**
     *
     * @return Meal Menu
     */
    public MealMenu mealMenu() {
        return this.mealMenu;
    }

    /**
     *
     * @return dish
     */
    public Dish dish() {
        return this.dish;
    }

    /**
     * @return type of the meal
     */
    public MealsType mealType() {
        return this.mealType;
    }

    /**
     *
     * @return day for the meal
     */
    public Calendar day() {
        return this.day;
    }

    @Override
    public boolean sameAs(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean is(MealMenu id) {
        return id.equals(mealMenu);
    }

    @Override
    public MealMenu id() {
        return this.mealMenu;
    }
}
