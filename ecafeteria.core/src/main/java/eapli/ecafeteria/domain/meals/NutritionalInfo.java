/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.ValueObject;
import eapli.util.Strings;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;

/**
 *
 * @author Luís Maia
 */
@Embeddable
public class NutritionalInfo implements ValueObject, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Double calorias;
    private Double sal;
    private Double gordura;

    /**
     * Default constructor
     */
    public NutritionalInfo() {
    }

    public NutritionalInfo(Double calorias, Double sal, Double gordura) {
        this.calorias = calorias;
        this.sal = sal;
        this.gordura = gordura;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.calorias);
        hash = 67 * hash + Objects.hashCode(this.sal);
        hash = 67 * hash + Objects.hashCode(this.gordura);
        return hash;
    }

    @Override
    public String toString() {
        return "calories(kcal)=" + calorias + ", salt(g)=" + sal + ", fat(g)=" + gordura;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NutritionalInfo other = (NutritionalInfo) obj;
        if (!Objects.equals(this.calorias, other.calorias)) {
            return false;
        }
        if (!Objects.equals(this.sal, other.sal)) {
            return false;
        }
        if (!Objects.equals(this.gordura, other.gordura)) {
            return false;
        }
        return true;
    }

}
