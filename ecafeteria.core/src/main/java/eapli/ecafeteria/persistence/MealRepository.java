/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;

/**
 *
 * @author Ana
 */
public interface MealRepository extends Repository<Meal, Long> {
    
    public Meal meals(MealMenu m);
    
    public Iterable<Meal> listMeal(Calendar day);
}
