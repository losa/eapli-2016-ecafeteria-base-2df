/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.framework.persistence.repositories.Repository;

/**
 *
 * @author Bruno
 */
public interface ReservationsRepository extends Repository<Reservation, Long>{
    
    public void saveReservations(Reservation r);

    public Iterable<Reservation> futureReservations(int days);

    public Iterable<Reservation> previousReservations();
    
     public Iterable<Reservation> rateableReservations();
     
    public boolean cancelReservation(Reservation r);
     
    
}
