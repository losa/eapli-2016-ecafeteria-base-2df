/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Ana
 */
class InMemoryMealPlanRepository extends InMemoryRepository<MealPlan, Long> implements MealPlanRepository {

    long nextID = 1;

    @Override
    protected Long newPK(MealPlan entity) {
        return ++nextID;
    }

}
