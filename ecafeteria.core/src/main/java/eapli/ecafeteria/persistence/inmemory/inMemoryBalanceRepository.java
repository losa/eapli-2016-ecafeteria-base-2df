/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.mealbooking.Saldo;
import eapli.ecafeteria.persistence.BalanceRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Asus
 */
public class inMemoryBalanceRepository extends InMemoryRepository<Saldo, Long> implements BalanceRepository {

    long nextID = 1;
    
    @Override
    protected Long newPK(Saldo entity) {
        return ++nextID;
    }  
    
}
