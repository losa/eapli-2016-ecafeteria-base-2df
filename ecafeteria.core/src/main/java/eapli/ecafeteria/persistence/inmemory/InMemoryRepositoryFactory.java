package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.persistence.AccountRepository;
import eapli.ecafeteria.persistence.BalanceRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealsTypeRepository;
import eapli.ecafeteria.persistence.MovimentoRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.POSRepository;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.ecafeteria.persistence.ReservationsRepository;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.jpa.JpaRatingRepository;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    private static UserRepository userRepository = null;
    private static DishTypeRepository dishTypeRepository = null;
    private static OrganicUnitRepository organicUnitRepository = null;
    private static CafeteriaUserRepository cafeteriaUserRepository = null;
    private static SignupRequestRepository signupRequestRepository = null;
    private static DishRepository dishRepository = null;
    private static MealsTypeRepository mealsTypeRepository = null;
    private static MealRepository mealRepository = null;
    private static ReservationsRepository reservationsRepository = null;
    private static MealMenuRepository mealMenuRepository = null;
    private static POSRepository posRepository = null;
    private static MovimentoRepository movRepository = null;
    private static AccountRepository accRepository = null;
    private static MealPlanRepository mealPlanRepository = null;
    private static MealPlanItemRepository mealPlanItemRepository = null;

    private static RatingRepository mealRatingRepository = null;

    private static BalanceRepository balanceRepository = null;


    @Override
    public UserRepository users() {
        if (userRepository == null) {
            userRepository = new InMemoryUserRepository();
        }
        return userRepository;
    }

    @Override
    public DishTypeRepository dishTypes() {
        if (dishTypeRepository == null) {
            dishTypeRepository = new InMemoryDishTypeRepository();
        }
        return dishTypeRepository;
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        if (organicUnitRepository == null) {
            organicUnitRepository = new InMemoryOrganicUnitRepository();
        }
        return organicUnitRepository;
    }

    @Override
    public CafeteriaUserRepository cafeteriaUsers() {

        if (cafeteriaUserRepository == null) {
            cafeteriaUserRepository = new InMemoryCafeteriaUserRepository();
        }
        return cafeteriaUserRepository;
    }

    @Override
    public SignupRequestRepository signupRequests() {

        if (signupRequestRepository == null) {
            signupRequestRepository = new InMemorySignupRequestRepository();
        }
        return signupRequestRepository;
    }

    @Override
    public DishRepository dish() {
        if (dishRepository == null) {
            dishRepository = new InMemoryDishRepository();
        }
        return dishRepository;
    }

    @Override
    public MealsTypeRepository mealsType() {
        if (mealsTypeRepository == null) {
            mealsTypeRepository = new InMemoryMealsTypeRepository();
        }
        return mealsTypeRepository;
    }

    @Override
    public MealRepository meal() {
        if (mealRepository == null) {
            mealRepository = new InMemoryMealRepository();
        }
        return mealRepository;
    }

    @Override
    public ReservationsRepository reservations() {
        if (reservationsRepository == null) {
            reservationsRepository = new InMemoryReservationsRepository();
        }
        return reservationsRepository;
    }

    @Override
    public MealMenuRepository mealMenu() {
        if (mealMenuRepository == null) {
            mealMenuRepository = new InMemoryMealMenuRepository();
        }
        return mealMenuRepository;
    }

    @Override
    public POSRepository pointOfSale() {
        if (posRepository == null) {
            posRepository = new InMemoryPOSRepository();
        }
        return posRepository;
    }

    @Override
    public MovimentoRepository movimento() {
        if (movRepository == null) {
            movRepository = new InMemoryMovimentoRepository();
        }
        return movRepository;
    }

    @Override
    public AccountRepository account() {
        if (accRepository == null) {
            accRepository = new InMemoryAccountRepository();
        }
        return accRepository;
    }
   
    @Override
    public MealPlanRepository mealPlan() {
        if (mealPlanRepository == null) {
            mealPlanRepository = new InMemoryMealPlanRepository();
        }
        return mealPlanRepository;
    }

    @Override
    public MealPlanItemRepository mealPlanItem() {
        if (mealPlanItemRepository == null) {
            mealPlanItemRepository = new InMemoryMealPlanItemRepository();
        }
        return mealPlanItemRepository;
    }
    
    @Override
    public BalanceRepository balance() {
        if (balanceRepository == null) {
            balanceRepository = new inMemoryBalanceRepository();
        }
        return balanceRepository;
    }

    @Override
    public RatingRepository ratings() {
        if(mealRatingRepository == null){
            mealRatingRepository = new InMemoryRatingRepository();
        }
        return mealRatingRepository;
    }

}
