/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Ana
 */
class InMemoryMealPlanItemRepository extends InMemoryRepository<MealPlanItem, Long> implements MealPlanItemRepository {

    long nextID = 1;

    @Override
    protected Long newPK(MealPlanItem entity) {
        return ++nextID;
    }
}
