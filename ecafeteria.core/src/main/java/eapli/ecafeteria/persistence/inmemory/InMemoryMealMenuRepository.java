/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Luís Maia
 */
public class InMemoryMealMenuRepository extends InMemoryRepository<MealMenu, Long> implements MealMenuRepository {

    long nextID = 1;

    @Override
    protected Long newPK(MealMenu entity) {
        return ++nextID;
    }

    @Override
    public Iterable<MealMenu> activeMealMenu() {
        return repository.values().stream().filter(e -> e.isActive()).collect(Collectors.toList());
    }

    
    public List<MealMenu> listMealMenu(Calendar Start, Calendar end) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
