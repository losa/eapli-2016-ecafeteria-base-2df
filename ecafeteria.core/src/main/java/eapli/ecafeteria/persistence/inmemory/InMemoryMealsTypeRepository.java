/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealsType;
import eapli.ecafeteria.persistence.MealsTypeRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Bolt
 */
public class InMemoryMealsTypeRepository extends InMemoryRepository<MealsType, Long> implements MealsTypeRepository{

    long nextID = 1;

    @Override
    protected Long newPK(MealsType entity) {
        return ++nextID;
    }
    
}
