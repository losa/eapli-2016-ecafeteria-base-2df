/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import eapli.ecafeteria.persistence.POSRepository;
import java.util.stream.Collectors;

/**
 *
 * @author Asus
 */
public class InMemoryPOSRepository extends InMemoryRepository<POS, Long> implements POSRepository {

    long nextID = 1;
    
    @Override
    protected Long newPK(POS entity) {
        return ++nextID;
    }    
}
