/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.mealbooking.Movimento;
import eapli.ecafeteria.persistence.MovimentoRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author RP
 */
public class InMemoryMovimentoRepository extends InMemoryRepository<Movimento, Long> implements MovimentoRepository  {
    
 long nextID = 1;

    @Override
    protected Long newPK(Movimento entity) {
        return ++nextID;
    }   
    
    }


