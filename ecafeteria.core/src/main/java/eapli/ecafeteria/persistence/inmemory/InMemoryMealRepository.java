/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;
import java.util.stream.Collectors;

/**
 *
 * @author Ana
 */
class InMemoryMealRepository extends InMemoryRepository<Meal, Long> implements MealRepository {

    long nextID = 1;

    @Override
    protected Long newPK(Meal entity) {
        return ++nextID;
    }

    @Override
    public Meal meals(MealMenu m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public Iterable<Meal> listMeal(Calendar day) {
        return repository.values().stream().filter(e -> e.day().compareTo(day)==0).collect(Collectors.toList());
    }

}
