/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Rating;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Nuno Barbosa 1140372 SIGMA
 */
public class InMemoryRatingRepository extends InMemoryRepository<Rating, Long>implements RatingRepository {

    long nextID = 1;
    
    @Override
    protected Long newPK(Rating entity) {
        return ++nextID;
    }

}
