/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.persistence.ReservationsRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author Bruno
 */
public class InMemoryReservationsRepository extends InMemoryRepository<Reservation, Long> implements ReservationsRepository {

    long nextID = 1;

    @Override
    protected Long newPK(Reservation entity) {
        return ++nextID;
    }

    @Override
    public Iterable<Reservation> futureReservations(int days) {
        //TODO get from the persistence and return the lsit of future reservations
        //get the current user from Session
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public Iterable<Reservation> previousReservations() {
        //TODO get from the persistence and return the lsit of previous reservations
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public void saveReservations(Reservation r) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Reservation> rateableReservations() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean cancelReservation(Reservation res) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
