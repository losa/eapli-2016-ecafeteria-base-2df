/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.framework.persistence.repositories.Repository;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Luís Maia
 */
public interface MealMenuRepository extends Repository<MealMenu, Long> {

    public Iterable<MealMenu> activeMealMenu();
    
    public List<MealMenu> listMealMenu(Calendar Start,Calendar end);
    
    
}
