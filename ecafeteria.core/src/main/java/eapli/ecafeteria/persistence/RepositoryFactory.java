/**
 *
 */
package eapli.ecafeteria.persistence;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface RepositoryFactory {

	UserRepository users();
        DishRepository dish();
	DishTypeRepository dishTypes();
        MealsTypeRepository mealsType();
        OrganicUnitRepository organicUnits();
        CafeteriaUserRepository cafeteriaUsers();
        SignupRequestRepository signupRequests();
        MealRepository meal();
        ReservationsRepository reservations();
        MealMenuRepository mealMenu();
        MealPlanRepository mealPlan();
        MealPlanItemRepository mealPlanItem();
        POSRepository pointOfSale();
        MovimentoRepository movimento();
        AccountRepository account();
        RatingRepository ratings();
        BalanceRepository balance();
}
