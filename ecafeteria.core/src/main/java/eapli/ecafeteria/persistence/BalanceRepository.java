/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.mealbooking.Saldo;
import eapli.framework.persistence.repositories.Repository;

/**
 *
 * @author Asus
 */
public interface BalanceRepository extends Repository<Saldo,Long>{
    
}
