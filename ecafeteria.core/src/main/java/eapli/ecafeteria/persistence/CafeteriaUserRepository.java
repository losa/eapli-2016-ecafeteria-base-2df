package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.Repository;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt 02/04/2016
 */
public interface CafeteriaUserRepository extends Repository<CafeteriaUser, MecanographicNumber> {
    
    /*@Override
    public boolean add(CafeteriaUser entity) throws DataIntegrityViolationException;
    
    @Override
    public CafeteriaUser save(CafeteriaUser entity);
        
    @Override
    public long size();
    
    @Override
    public Iterable<CafeteriaUser> all();*/
    
    public CafeteriaUser findBySystemUser(SystemUser s);
}
