/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.mealbooking.Movimento;
import eapli.ecafeteria.persistence.MovimentoRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

/**
 *
 * @author RP
 */
public class JpaMovimentoRepository extends JpaRepository<Movimento, Long> implements MovimentoRepository{

    @Override
    protected String persistenceUnitName() {
         return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
}
