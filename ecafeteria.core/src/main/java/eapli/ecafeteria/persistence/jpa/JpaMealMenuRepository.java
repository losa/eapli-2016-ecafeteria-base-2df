/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Luís Maia
 */
public class JpaMealMenuRepository extends JpaRepository<MealMenu, Long> implements MealMenuRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public Iterable<MealMenu> activeMealMenu() {
        return match("e.published=true");
    }
   
    @Override
    public List<MealMenu> listMealMenu(Calendar Start, Calendar end) {
        List<MealMenu> list = all();
        List<MealMenu> aux = new ArrayList<MealMenu>();
        System.out.println("Numero de ref: "+list.size());
        for (MealMenu mealMenu : list) {
            if ((Start.compareTo(mealMenu.period().firstDay()) <= 0)
                    && (end.compareTo(mealMenu.period().finalDay()) >= 0)) {
                aux.add(mealMenu);
            }
        }
        return aux;
    }
}
