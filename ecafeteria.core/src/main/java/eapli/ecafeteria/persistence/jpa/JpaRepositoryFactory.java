package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.persistence.BalanceRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealsTypeRepository;
import eapli.ecafeteria.persistence.MovimentoRepository;
import eapli.ecafeteria.persistence.POSRepository;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservationsRepository;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.UserRepository;

/**
 *
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {
    
    @Override
    public UserRepository users() {
        return new JpaUserRepository();
    }

    @Override
    public JpaDishTypeRepository dishTypes() {
        return new JpaDishTypeRepository();
    }

    @Override
    public JpaAccountRepository account() {
        return new JpaAccountRepository();
    }

    @Override
    public MealsTypeRepository mealsType() {
        return new JpaMealsTypeRepository();
    }

    @Override
    public JpaOrganicUnitRepository organicUnits() {
        return new JpaOrganicUnitRepository();
    }

    @Override
    public JpaCafeteriaUserRepository cafeteriaUsers() {
        return new JpaCafeteriaUserRepository();
    }

    @Override
    public SignupRequestRepository signupRequests() {
        return new JpaSignupRequestRepository();
    }

    @Override
    public DishRepository dish() {
        return new JpaDishRepository();
    }

    @Override
    public MealRepository meal() {
        return new JpaMealRepository();
    }

    @Override
    public ReservationsRepository reservations() {
        return new JpaReservationsRepository();
    }

    @Override
    public MealMenuRepository mealMenu() {
        return new JpaMealMenuRepository();
    }

    @Override
    public POSRepository pointOfSale() {
        return new JpaPOSRepository();
    }

    @Override
    public MovimentoRepository movimento() {
        return new JpaMovimentoRepository();
    }

    @Override
    public MealPlanRepository mealPlan() {
        return new JpaMealPlanRepository();
    }

    @Override
    public MealPlanItemRepository mealPlanItem() {
        return new JpaMealPlanItemRepository();
    }

    @Override
    public BalanceRepository balance() {
        return new JpaBalanceRepository();
    }
    
    public RatingRepository ratings() {
        return new JpaRatingRepository();
    }
}
