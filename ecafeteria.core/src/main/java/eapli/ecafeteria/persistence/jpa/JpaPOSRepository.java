/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import eapli.ecafeteria.persistence.POSRepository;

/**
 *
 * @author Asus
 */
public class JpaPOSRepository extends JpaRepository<POS, Long> implements POSRepository{

    @Override
    protected String persistenceUnitName() {
         return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
    
}
