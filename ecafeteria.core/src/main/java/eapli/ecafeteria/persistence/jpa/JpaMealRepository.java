/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.Calendar;
import java.util.List;
import javax.persistence.TemporalType;

/**
 *
 * @author Ana
 */
public class JpaMealRepository extends JpaRepository<Meal, Long> implements MealRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    public Meal meals(MealMenu menu) {
        List<Meal> m_meal = all();
        for (Meal m : m_meal) {
            if (m.is(menu)) {
                return m;
            }
        }
        return null;

    }

    @Override
    public Iterable<Meal> listMeal(Calendar day) {
        return entityManager().createQuery(
                "SELECT m FROM Meal m WHERE m.day LIKE :date")
                .setParameter("date", day, TemporalType.DATE)
                .getResultList();

    }

}
