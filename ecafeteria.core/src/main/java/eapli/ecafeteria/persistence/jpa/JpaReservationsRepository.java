/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.ReservationsRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Bruno
 */
public class JpaReservationsRepository extends JpaRepository<Reservation, Long> implements ReservationsRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public void saveReservations(Reservation r) {       
    }

    @Override
    public Iterable<Reservation> futureReservations(int days) {
        SystemUser sysUser = AppSettings.instance().session().authenticatedUser();
        CafeteriaUser cafu = PersistenceContext.repositories().cafeteriaUsers().findBySystemUser(sysUser);
        String query = "SELECT * From Reservation where user=" + cafu.mecanographicNumber().toString();
        List<Reservation> usr_resrvs = executeQuery(query);
        Calendar cal = Calendar.getInstance();
        Calendar calMax = Calendar.getInstance();
        calMax.add(Calendar.DATE, days);
        List<Reservation> ret = new ArrayList();
        for (Reservation r : usr_resrvs) {
            if (r.Meal().day().after(cal) && r.Meal().day().before(calMax)) {
                ret.add(r);
            }
        }
        return ret;
    }

    @Override
    public Iterable<Reservation> previousReservations() {
        SystemUser sysUser = AppSettings.instance().session().authenticatedUser();
        CafeteriaUser cafu = PersistenceContext.repositories().cafeteriaUsers().findBySystemUser(sysUser);
        String query = "SELECT * From Reservation where ac=" + cafu.mecanographicNumber().toString();
        List<Reservation> usr_resrvs = executeQuery(query);
        Calendar cal = Calendar.getInstance();
        List<Reservation> ret = new ArrayList();
        for (Reservation r : usr_resrvs) {
            if (r.Meal().day().before(cal)) {
                ret.add(r);
            }
        }
        return ret;
    }

    @Override
    public Iterable<Reservation> rateableReservations() {
        return match("e.status=2");
    }

    @Override
    public boolean cancelReservation(Reservation r) {          
        super.entityManager().getTransaction().begin();
        Reservation aux = super.entityManager().merge(r);
        super.entityManager().remove(aux);        
        super.entityManager().getTransaction().commit();
        return true;
    }

   
}
