package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.Repository;

/**
 * Created by nuno on 21/03/16.
 */
public interface UserRepository extends Repository<SystemUser, Username> {
    
    @Override
    public boolean add(SystemUser entity) throws DataIntegrityViolationException;
    
    @Override
    public SystemUser save(SystemUser entity);
    
    @Override
    public SystemUser findById(Username id);
    
    @Override
    public long size();
    
    @Override
    public Iterable<SystemUser> all();
    
}
