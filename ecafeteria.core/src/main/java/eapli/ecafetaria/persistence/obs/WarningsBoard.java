/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.persistence.obs;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import java.util.Observable;

/**
 *
 * @author Bruno
 */
public class WarningsBoard extends Observable{
    
    private double percentage;
   
    /*public void updateStatus(MealPlanItem mpi){
        percentage = mpi.avaiableQtt()/mpi.intendedQtt();
        System.out.println("PERCENTAGE = "+this.percentage+"\n");
        if(percentage>=90){
            setChanged();
            notifyObservers("*Alerta Vermelho* - pelo menos 90% das refeições disponíveis "
                    + "já foram reservadas!");
        }else if(percentage>=75){
            setChanged();
            notifyObservers("*Alerta Amarelo* - pelo menos 75% das refeições disponíveis "
                    + "foram reservada!");
        }
    }*/
    
    public String updateStatus(MealPlanItem mpi) {
        if(mpi.intendedQtt() == 0) {
            return null;
        }
        percentage = (double)(mpi.avaiableQtt()/mpi.intendedQtt())*100;
        if(percentage>=90){
            setChanged();
            return "*Alerta Vermelho* - pelo menos 90% das refeições disponíveis da refeição "
                    + mpi.meal().dish() + " do plano " + mpi.mealPlan().descricao() + "já foram reservadas!";
        }else if(percentage>=75){
            setChanged();
            return "*Alerta Amarelo* - pelo menos 75% das refeições disponíveis da refeição "
                    + mpi.meal().dish() + " do plano " + mpi.mealPlan().descricao() + "já foram reservadas!";
        }
        return null;
    }
}
