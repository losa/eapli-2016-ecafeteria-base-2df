/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco Miranda
 */
public class NutritionalInfoTest {
    
    private NutritionalInfo instance = new NutritionalInfo(200.0, 20.0, 15.0);
    private NutritionalInfo instance2 = new NutritionalInfo(200.0, 20.0, 15.0);
    public NutritionalInfoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of hashCode method, of class NutritionalInfo.       Nao e necessario
     */
//    @Test
//    public void testHashCode() {
//        System.out.println("hashCode");
//        NutritionalInfo instance = new NutritionalInfo();
//        int expResult = 0;
//        int result = instance.hashCode();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of toString method, of class NutritionalInfo.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String result = instance.toString();
        String expResult = "calories(kcal)=200.0, salt(g)=20.0, fat(g)=15.0";
        assertEquals(result,expResult);
    }

    /**
     * Test of equals method, of class NutritionalInfo.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Boolean result = instance.equals(instance2);
        Boolean expResult = true;
        assertEquals(result,expResult);
    }
}
