/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco Miranda
 */
public class DishTest {
    
    public DishTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of name method, of class Dish.
     */
    @Test
    public void testAtributes() {
        //test name, price, calorias, sal, gordura, dishType
        System.out.println("Teste dos atributos\n");
        DishType dt = new DishType("tipoTeste", "descricaoTeste");
        DishType dtTrocado = new DishType("trocado", "desc");
        NutritionalInfo nt = new NutritionalInfo(100.0,10.,100.0);
        Dish d = new Dish("nome", 5.0,100.0,10.0,10.0,dt);
        DishName dn = new DishName("nome");
        DishName dnTrocado = new DishName("nomeTrocado");           //expected = tipoTeste   result trocado
        
        String dishTypeAcronym = "tipoTeste";
        String dishTypeAcronymTrocado = "trocado";
        String dishNameTrocado = "nomeTrocado";
        
        String nutriExpres = "calories(kcal)=100.0, salt(g)=10.0, fat(g)=100.0";
        String nutriRes = nt.toString();
        
        Double priceExpres = Double.parseDouble(d.price());
        Double priceRes = 5.0;
        
        Boolean estado = true;
        Boolean estadoTrocado = false;
        Boolean isName = true;
        
       // d.changeDishState();
        assertEquals(nutriExpres, nutriRes);        //nutinfo
        assertEquals(priceExpres, priceRes);        //price
        assertEquals(dt.id(), dishTypeAcronym);     //dishtype -> acronym is unique
        assertEquals(d.isActive(), estado);         // isActive
        
        d.changeDishState();
        assertEquals(d.isActive(), estadoTrocado);       //change state
        
        d.addDishType(dtTrocado);
        assertEquals(d.dishType(), dishTypeAcronymTrocado);           //add dish type
        assertEquals(d.id(), dn.toString());                //test id -> nome do dish
        assertEquals(d.is(dn.toString()),isName);                         //test is
        
        d.changeNameTo(dishNameTrocado);
        assertEquals(d.name(), dishNameTrocado);       //test changeNameTo

    }



    //Ainda não implementado
    
//    /**
//     * Test of sameAs method, of class Dish.
//     */
//    @Test
//    public void testSameAs() {
//        System.out.println("sameAs");
//        Object other = null;
//        Dish instance = new Dish();
//        boolean expResult = false;
//        boolean result = instance.sameAs(other);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
