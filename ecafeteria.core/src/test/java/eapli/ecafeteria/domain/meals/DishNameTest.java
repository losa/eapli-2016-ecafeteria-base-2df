/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class DishNameTest {

    public DishNameTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test(expected = IllegalStateException.class)
    public void testNameMustNotBeEmpty() {
        System.out.println("must have non-empty name");
        new DishName("");
    }

    @Test(expected = IllegalStateException.class)
    public void testNameMustNotBeNull() {
        System.out.println("must have an name");
        new DishName(null);
    }

//    /**
//     * Test of hashCode method, of class DishName.
//     */
//    @Test
//    public void testHashCode() {
//        System.out.println("hashCode");
//        DishName instance = null;
//        int expResult = 0;
//        int result = instance.hashCode();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of equals method, of class DishName.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        final String name="Feijoada";
        Object o = new DishName(name);
        DishName instance = new DishName(name);
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
        
        Object o1= new DishName("Bacalhau!");
        boolean expResult1=false;
        boolean result1=instance.equals(o1);
        assertEquals(expResult1,result1);
    }

    /**
     * Test of toString method, of class DishName.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        final String name="Feijoada";
        DishName instance = new DishName(name);
        String expResult = name;
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
