/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.TimePeriod2;
import eapli.util.Console;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class MealMenuTest {
    DishType dtype= new DishType("Alm","Almoço");
    Dish dish= new Dish("Feijoada",5.00,210.0,6.0,5.0,dtype);
    MealsType mtype= new MealsType("veg","Vegetarian");
    Dish dish1=null;
    MealsType mtype1= null;
    Calendar begin = new GregorianCalendar(2016,5,17);
    Calendar end = new GregorianCalendar(2016,5,18);
    String descricao= "teste meal menu!";
    MealMenu instance=new MealMenu(begin,end,descricao);
    Meal meal= new Meal(dish,mtype,begin,instance);
    /*
    public MealMenuTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDishAndMealTypeNotBeNull() {
        System.out.println("must have an Dish");
        instance.add(dish1,mtype,begin);
        
        System.out.println("must have an MealType and an Dish");
        instance.add(dish1,mtype1,begin);
        
        System.out.println("must have an mealType");
        instance.add(dish,mtype1,begin);
    }

    /**
     * Test of add method, of class MealMenu.
     */
    /*
    @Test
    public void testAdd() {
        System.out.println("add");
        instance.add(dish, mtype, begin);
    }*/

    /**
     * Test of descricao method, of class MealMenu.
     */
    /*
    @Test
    public void testDescricao() {
        System.out.println("descricao");
        String expResult = descricao;
        String result = instance.descricao();
        assertEquals(expResult, result);
    }*/

    /**
     * Test of period method, of class MealMenu.
     */
    /*
    @Test
    public void testPeriod() {
        System.out.println("period");
        TimePeriod2 expResult = new TimePeriod2 (begin,end);
        TimePeriod2 result = instance.period();
        assertEquals(expResult, result);
    }*/

    /**
     * Test of sameAs method, of class MealMenu.
     */
    /*
    @Test
    public void testSameAs() {
        System.out.println("sameAs");
        Object other = null;
        MealMenu instance = new MealMenu();
        boolean expResult = false;
        boolean result = instance.sameAs(other);
        assertEquals(expResult, result);
    }*/

    /**
     * Test of is method, of class MealMenu.
     */
    /*
    @Test
    public void testIs() {
        System.out.println("is");
        boolean expResult = true;
        boolean result = instance.is(meal);
        assertEquals(expResult, result);
    }*/

    /**
     * Test of id method, of class MealMenu.
     */
    /*
    @Test
    public void testId() {
        System.out.println("id");
        MealMenu instance = new MealMenu();
        Meal expResult = null;
        Meal result = instance.id();
        assertEquals(expResult, result);
    }
    */
}
