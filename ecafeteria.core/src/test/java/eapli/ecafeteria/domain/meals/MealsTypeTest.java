/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class MealsTypeTest {
    MealsType instance = new MealsType("Alm","Almoço");
    public MealsTypeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAcronymMustNotBeEmpty() {
        System.out.println("must have non-empty acronym");
        new MealsType("", "Almoço");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAcronymMustNotBeNull() {
        System.out.println("must have an acronym");
        new MealsType(null, "Almoço");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescriptionMustNotBeEmpty() {
        System.out.println("must have non-empty description");
        new MealsType("Alm", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDescriptionMustNotBeNull() {
        System.out.println("must have a description");
        new MealsType("Alm", null);
    }

    /**
     * Test of description method, of class MealsType.
     */
    @Test
    public void testDescription() {
        System.out.println("description");
        String expResult = "Almoço";
        String result = instance.description();
        assertEquals(expResult, result);
    }
    /**
     * Test of changeDescriptionTo method, of class MealsType.
     */

    @Test(expected = IllegalArgumentException.class)
    public void testchangeDescriptionToMustNotBeNull() {
        System.out.println("ChangeDescriptionTo -New description must not be null");
        instance.changeDescriptionTo(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testchangeDescriptionToMustNotBeEmpty() {
        System.out.println("ChangeDescriptionTo -New description must not be empty");
        instance.changeDescriptionTo("");
    }

    @Test
    public void testchangeDescriptionTo() {
        System.out.println("attest changeDescriptionTo");
        final String newDescription = "new description";
        instance.changeDescriptionTo(newDescription);
        final String expResult = newDescription;
        final String result = instance.description();
        assertEquals(expResult, result);
    }

//    /**
//     * Test of sameAs method, of class MealsType.
//     */
//    @Test
//    public void testSameAs() {
//        System.out.println("sameAs");
//        Object other = new MealsType("Alm","Almoço");
//        Object other1= new MealsType("Jan","Jantar");
//        boolean expResult = true;
//        boolean result = instance.sameAs(other);
//        assertEquals(expResult, result);
//        
//        boolean expResult1= false;
//        boolean result1=instance.sameAs(other1);
//        assertEquals(expResult1,result1);
//    }

    /**
     * Test of is method, of class MealsType.
     */
    @Test
    public void testIs() {
        System.out.println("is");
        String id = "Alm";
        String id1= "Jan";
        boolean expResult = true;
        boolean result = instance.is(id);
        assertEquals(expResult, result);
        
        boolean expResult1=false;
        boolean result1=instance.is(id1);
        assertEquals(expResult1,result1);
    }

    /**
     * Test of id method, of class MealsType.
     */
    @Test
    public void testId() {
        System.out.println("id");
        final String id="Alm";
        MealsType ins1= new MealsType(id,"Description");
        final String expResult=id;
        String result = ins1.id();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class MealsType.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new MealsType("Jan","Jantar");
        Object o1= new MealsType("Alm","Almoço");
        boolean expResult = false;
        boolean expResult1= true;
        boolean result = instance.equals(obj);
        boolean result1=instance.equals(o1);
        assertEquals(expResult, result);
        assertEquals(expResult1,result1);
    }
    
}
