/**
 *
 */
package eapli.framework.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Paulo Gandra Sousa
 *
 */
@Embeddable
public class TimePeriod2 implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    @Temporal(TemporalType.DATE)
    private Calendar start;
    @Temporal(TemporalType.DATE)
    private Calendar end;

    public TimePeriod2(Calendar start, Calendar end) {
        this.start = start;
        this.end = end;
    }

    public TimePeriod2() {
    }
    
    public Calendar firstDay () {
        return this.start;
    }

    public Calendar finalDay () {
        return this.end;
    }
    
    public String converter(Calendar dia) {
        
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date teste = dia.getTime();
        String tipo_data = df.format(teste);
        
        return tipo_data;
    }
}
