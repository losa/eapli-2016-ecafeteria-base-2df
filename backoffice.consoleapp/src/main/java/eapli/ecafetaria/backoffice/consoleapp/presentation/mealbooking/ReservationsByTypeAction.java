/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.backoffice.consoleapp.presentation.mealbooking;

import eapli.framework.actions.Action;

/**
 *
 * @author Bruno
 */
public class ReservationsByTypeAction implements Action {
    @Override
    public boolean execute() {
        return new ReservationsByTypeUI().show();
    }
}
