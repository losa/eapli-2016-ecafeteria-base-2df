/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.backoffice.consoleapp.presentation.mealbooking;

import eapli.ecafeteria.application.ListReservationsController;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.mealbooking.rSortType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import java.util.List;

/**
 *
 * @author Bruno
 */
public class ReservationsByMealUI extends AbstractUI{
    
    private final ListReservationsController theController = new ListReservationsController();
    private final rSortType sort_type = rSortType.MEAL;
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final List<Reservation> list = this.theController.listReservations(sort_type);

        if (!list.isEmpty()) {
            for(Reservation r: list){
                System.out.println(r.toString());
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String headline() {
        return "List reservations sorted by Meal";
    }
}