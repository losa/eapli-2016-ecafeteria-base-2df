/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Ana
 */
public class MealPlanPrinter implements Visitor<MealPlan> {

    @Override
    public void visit(MealPlan visitee) {
        System.out.printf("%-30s\n", visitee.descricao());
    }

    @Override
    public void beforeVisiting(MealPlan visitee) {

    }

    @Override
    public void afterVisiting(MealPlan visitee) {

    }

}
