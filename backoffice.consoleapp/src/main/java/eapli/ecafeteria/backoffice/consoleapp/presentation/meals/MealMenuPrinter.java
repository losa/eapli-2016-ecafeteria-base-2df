/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Bolt
 */
public class MealMenuPrinter implements Visitor<MealMenu> {

    @Override
    public void visit(MealMenu visitee) {
        System.out.printf("%-30s\n", visitee.descricao());
    }

    @Override
    public void beforeVisiting(MealMenu visitee) {
        
    }

    @Override
    public void afterVisiting(MealMenu visitee) {
        
    }
    
}
