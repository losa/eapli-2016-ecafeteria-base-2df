/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.CreatePlanMenuController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author Bolt
 */
public class CreatePlanMenuUI extends AbstractUI {

    private final CreatePlanMenuController theController = new CreatePlanMenuController();

    protected Controller controller() {
        return this.theController;
    }

    public CreatePlanMenuUI() {
    }

    @Override
    protected boolean doShow() {
        final String descricao = Console.readLine("Insert the description for the Plan menu:");
        final Calendar begin = Console.readCalendar("Insert the first day for the plan: (dd-mm-yyyy)");
        final Calendar end = Console.readCalendar("Insert the last day for the plan: (dd-mm-yyyy)");

        try {
            this.theController.CreateMealPlanController(descricao, begin, end);
        } catch (final DataIntegrityViolationException e) {
            System.out.println("That date is already in use.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Create Meal Plan";
    }

}
