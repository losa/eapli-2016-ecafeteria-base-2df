/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cashier;

import eapli.framework.actions.Action;

/**
 *
 * @author SHAWSHANK
 */
public class ListAvailableDishesAction implements Action{

    @Override
    public boolean execute() {
        return new ListAvailableDishesUI().show();
    }
    
}
