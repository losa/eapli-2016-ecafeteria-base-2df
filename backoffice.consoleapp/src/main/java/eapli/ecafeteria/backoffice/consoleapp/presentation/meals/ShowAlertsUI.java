
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.AlertsController;
import eapli.framework.presentation.console.AbstractUI;
import java.util.Calendar;

public class ShowAlertsUI extends AbstractUI {
    
    private AlertsController controller = new AlertsController();

    @Override
    protected boolean doShow() {
        Iterable<String> alerts = controller.allAlertsFrom(Calendar.getInstance());

        if(alerts != null) {
            for(String alert : alerts) {
                System.out.println(alert);
            }
        } else {
            System.out.println("Não existem alertas.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Alerts";
    }
    
}
