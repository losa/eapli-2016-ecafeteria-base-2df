/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterMealsController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.domain.meals.MealsType;
import eapli.framework.application.Controller;
import eapli.framework.domain.TimePeriod2;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Luís Maia
 */
public class RegisterMealsUI extends AbstractUI {

    private final RegisterMealsController theController = new RegisterMealsController();
    private final TimePeriod2 tp = new TimePeriod2();
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        System.out.println("Select Menu");
        final SelectWidget<MealMenu> selectorMenu = new SelectWidget<>(this.theController.listMenus(),
                new MealMenuPrinter());
        selectorMenu.show();
        final MealMenu theMenu = selectorMenu.selectedElement();
        
        Calendar day = theMenu.period().firstDay();
        int res = intervaloDias(theMenu);
        String proximo = "X";

        String data = tp.converter(day);
        System.out.println("Preencha os dados do dia " + data);

        while (res > 0) {

            System.out.println("Select Type of Meal");
            final SelectWidget<MealsType> selectorMealType = new SelectWidget<>(this.theController.listMealsTypes(),
                    new MealsTypePrinter());
            selectorMealType.show();
            final MealsType theMealsType = selectorMealType.selectedElement();

            System.out.println("Select Dish");
            final SelectWidget<Dish> selectorDish = new SelectWidget<>(this.theController.listDish(),
                    new DishPrinter());
            selectorDish.show();
            final Dish theDish = selectorDish.selectedElement();

            try {
                this.theController.RegisterMealsController(theDish, theMealsType, day, theMenu);
            } catch (final DataIntegrityViolationException e) {
                System.out.println("That meal is already in use.");
            }

            while ( !proximo.equals("S") && !proximo.equals("N")) {
                proximo = Console.readLine("Prentede ir para o proximo dia: SIM(S) NAO(N)");
                if (proximo.equals("S")) {
                    res--;
                    day.add(Calendar.DAY_OF_YEAR, 1);
                    data = tp.converter(day);
                    System.out.println("Preencha os dados do dia " + data);
                }
            }
            
            proximo = "X";
            
            if (res == 0) {
                System.out.println("Ja preencheu o periodo todo");
            }

        }

        return false;
    }

    public Integer intervaloDias (MealMenu theMenu) {
        
        Calendar day = theMenu.period().firstDay();
        int inicio = day.get(Calendar.DAY_OF_YEAR);

        Calendar end = theMenu.period().finalDay();
        int fim = end.get(Calendar.DAY_OF_YEAR);

        int intervalo = fim - inicio + 1;
        
        return intervalo;
    }
    
    @Override
    public String headline() {
        return "Register Meal";
    }

}
