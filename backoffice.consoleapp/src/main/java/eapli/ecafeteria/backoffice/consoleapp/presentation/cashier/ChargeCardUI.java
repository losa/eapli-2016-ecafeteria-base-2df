/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cashier;

import eapli.ecafeteria.application.AccountChargeController;
import eapli.ecafeteria.application.AddUserController;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class ChargeCardUI extends AbstractUI {

    private final AccountChargeController theController = new AccountChargeController();

    @Override
    protected boolean doShow() {
                
        /*final CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers();
        Iterator it = cafeteriaUserRepository.all().iterator();
        while (it.hasNext()) {
            CafeteriaUser user = (CafeteriaUser) it.next();
            System.out.println("\nUSER: " + user.Name() + "\nUserNumber: " + user.id() + "\n");
        }*/
        try {
            String str = Console.readLine("Number: ");
            final MecanographicNumber number = new MecanographicNumber(str);
            Account account = this.theController.getAccountID(number);
            if (account != null) {
                final double saldo = Console.readDouble("Value to charge: ");
                this.theController.chargeAccount(saldo, account);
            } else {
                System.out.println("The number isn't associated with any user.\n");
            }
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(OpenPOSUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    @Override
    public String headline() {
        return "Charge Card.";
    }
}
