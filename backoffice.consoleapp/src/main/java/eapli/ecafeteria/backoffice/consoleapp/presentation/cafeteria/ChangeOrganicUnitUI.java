/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria;

import eapli.framework.presentation.console.AbstractUI;
import eapli.ecafeteria.application.ChangeOrganicUnitController;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;

/**
 *
 * @author Sofia
 */
public class ChangeOrganicUnitUI extends AbstractUI {

    private final ChangeOrganicUnitController theController = new ChangeOrganicUnitController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Iterable<OrganicUnit> list = this.theController.listOrganicUnits();
        final SelectWidget<OrganicUnit> selector = new SelectWidget<>(list, new OrganicUnitPrinter());
        selector.show();
        final OrganicUnit organicUnit = selector.selectedElement();

        final String name = Console.readLine("New name");
        final String description = Console.readLine("New description");

        try{
        this.theController.changeOrganicUnit(organicUnit, name, description);
        }catch (final DataIntegrityViolationException e){
            System.out.println("That name or description is invalid.");
        }
        return true;
    }

    @Override
    public String headline() {
        return "Change Organic Unit";
    }

}
