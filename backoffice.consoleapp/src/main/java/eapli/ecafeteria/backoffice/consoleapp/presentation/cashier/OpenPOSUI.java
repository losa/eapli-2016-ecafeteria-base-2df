/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cashier;

import eapli.ecafeteria.application.OpenPOSController;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class OpenPOSUI extends AbstractUI {

    private final OpenPOSController theController = new OpenPOSController();
    
    @Override
    protected boolean doShow() {
        try {
            this.theController.openPOS();
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(OpenPOSUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    @Override
    public String headline() {
        return "Open Cashier.";
    }
    
}
