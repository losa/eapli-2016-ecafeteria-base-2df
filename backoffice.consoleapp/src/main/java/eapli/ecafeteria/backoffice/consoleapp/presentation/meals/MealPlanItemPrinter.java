/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Ana
 */
public class MealPlanItemPrinter implements Visitor<MealPlanItem> {

    @Override
    public void visit(MealPlanItem visitee) {
        System.out.printf("%-30s\n", visitee.meal().dish().name());
    }

    @Override
    public void beforeVisiting(MealPlanItem visitee) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void afterVisiting(MealPlanItem visitee) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
