/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cashier;

import eapli.ecafeteria.application.ListAvailableDishesController;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.presentation.console.AbstractUI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author SHAWSHANK
 */
public class ListAvailableDishesUI extends AbstractUI {

    private final ListAvailableDishesController theController = new ListAvailableDishesController();

    @Override
    protected boolean doShow() {
        
        Map<Integer, MealPlanItem> mapDish = this.theController.availableDishes();
        if (!mapDish.isEmpty()) {
            Set<Integer> key = mapDish.keySet();
            System.out.println("KEY: " +key +"\n");
            for (Iterator<Integer> iterator = key.iterator(); iterator.hasNext();) {
                Integer k = iterator.next();
                System.out.println("KEY: "+ k);
                if (k != null) {
                    System.out.println("Dish: " + mapDish.get(k).meal().dish().dishType() + " Ammount: " + mapDish.get(k).avaiableQtt());
                }
            }
            return true;
        }else{
            System.out.println("None dish available.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "All Dishes";
    }

}
