/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterMealInformationController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;

/**
 *
 * @author Ardyvee
 */
public class RegisterMealsNotSoldUI extends AbstractUI {

    private final RegisterMealInformationController controller = new RegisterMealInformationController();

    @Override
    protected boolean doShow() {
        /*final Iterable<MealMenu> menus = controller.allMenus();
        final SelectWidget<MealMenu> selector = new SelectWidget<>(menus, new MealMenuPrinter()); //FIX ME
        selector.show();
        final MealMenu menu = selector.selectedElement();
        if (menu != null) {
            int number = -1;
            do {
                final String newDescription = Console
                        .readLine("Insert how many meals were made but not sold: ");
                try {
                    number = Integer.parseInt(newDescription);
                } catch (Exception ex) {
                    continue;
                }
            } while (number < 0);
            this.controller.mealsNotSold(number, menu); //FIX ME
        }*/
        return false;
    }

    @Override
    public String headline() {
        return "Register Meals Not Sold";
    }

}
