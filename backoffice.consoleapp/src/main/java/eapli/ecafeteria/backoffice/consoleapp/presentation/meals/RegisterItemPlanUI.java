/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterItemPlanController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.application.Controller;
import eapli.framework.domain.TimePeriod2;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author Bolt
 */
public class RegisterItemPlanUI extends AbstractUI{

    private final RegisterItemPlanController theController = new RegisterItemPlanController();
    private final TimePeriod2 tp = new TimePeriod2();
    
    protected Controller controller() {
        return this.theController;
    }
    
    @Override
    protected boolean doShow() {
        
        System.out.println("Select Plan");
        final SelectWidget<MealPlan> selectorMenu = new SelectWidget<>(this.theController.listPlan(),
                new MealPlanPrinter());
        selectorMenu.show();
        final MealPlan thePlan = selectorMenu.selectedElement();
        if(thePlan == null) {
            return true;
        }
        
        Calendar day = thePlan.period().firstDay();
        int res = intervaloDias(thePlan);
        String proximo = "X";

        String data = tp.converter(day);
        System.out.println("Fill the data for day " + data);
        
        while (res > 0) {

            System.out.println("Select Meal");
            final SelectWidget<Meal> selectorMealType = new SelectWidget<>(this.theController.listMeal(thePlan.period().firstDay()),
                    new MealPrinter());
            selectorMealType.show();
            final Meal theMeal = selectorMealType.selectedElement();
            if(theMeal == null) {
                return true;
            }

            final int intendedQtt = Console.readInteger("Numero de refeicoes previstas:");
            MealPlanItem mpi = new MealPlanItem(theMeal, thePlan, intendedQtt);
            try {
                this.theController.RegisterItemPlanController(mpi);
            } catch (final DataIntegrityViolationException e) {
                System.out.println("That meal is already in use.");
            }

            while ( !proximo.equalsIgnoreCase("S") && !proximo.equalsIgnoreCase("N")) {
                proximo = Console.readLine("Prentede ir para o proximo dia: SIM(S) NAO(N)");
                if (proximo.equalsIgnoreCase("S")) {
                    res--;
                    day.add(Calendar.DAY_OF_YEAR, 1);
                    data = tp.converter(day);
                    System.out.println("Preencha os dados do dia " + data);
                }
            }
            
            proximo = "X";
            
            if (res == 0) {
                System.out.println("Ja preencheu o periodo todo");
            }
        }
        return false;
    }

    
    public Integer intervaloDias (MealPlan thePlan) {
        
        Calendar day = thePlan.period().firstDay();
        int inicio = day.get(Calendar.DAY_OF_YEAR);

        Calendar end = thePlan.period().finalDay();
        int fim = end.get(Calendar.DAY_OF_YEAR);

        int intervalo = fim - inicio + 1;
        
        return intervalo;
    }
    
    
    @Override
    public String headline() {
        return "Register Plan";
    }
    
}
