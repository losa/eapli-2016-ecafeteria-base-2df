package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.framework.actions.Action;

public class DisplayCookedMealsAction implements Action {

    public DisplayCookedMealsAction() {
    }

    @Override
    public boolean execute() {
        return new DisplayCookedMealsUI().show();
    }
    
}
