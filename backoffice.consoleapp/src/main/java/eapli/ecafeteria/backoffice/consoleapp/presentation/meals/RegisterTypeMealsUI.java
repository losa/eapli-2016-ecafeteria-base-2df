/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterTypeMealsController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author Bolt
 */
public class RegisterTypeMealsUI extends AbstractUI{
    
    private final RegisterTypeMealsController theController= new RegisterTypeMealsController();

    protected Controller controller() {
        return this.theController;
    }
    
    @Override
    protected boolean doShow() {
        final String acronym = Console.readLine("Meals Type Acronym:");
        final String description = Console.readLine("Meals Type Description:");
        
        try {
            this.theController.registerMealsType(acronym, description);
        } catch (final DataIntegrityViolationException e) {
            System.out.println("That acronym is already in use.");
        }
        return false;
        
    }

    @Override
    public String headline() {
        return "Register Meals Type";
    }
    
}
