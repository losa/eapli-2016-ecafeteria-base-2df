/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterMealInformationController;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

/**
 *
 * @author Ardyvee
 */
public class DisplayCookedMealsUI extends AbstractUI {
    
    private final RegisterMealInformationController controller = new RegisterMealInformationController();

    @Override
    protected boolean doShow() {
        final Iterable<MealPlan> plans = controller.allMealPlans();
        final SelectWidget<MealPlan> selector = new SelectWidget<>(plans, new MealPlanPrinter());
        selector.show();
        final MealPlan plan = selector.selectedElement();
        if(plan == null) {
            return true;
        }

        final SelectWidget<MealPlanItem> select = new SelectWidget<>(this.controller.allMenuPlanItemsOfPlan(plan), new MealPlanItemPrinter());
        select.show();
        final MealPlanItem item = select.selectedElement();
        if(plan == null) {
            return true;
        }
        
        System.out.printf("Cooked meals: %d\n", item.cookedQtt());
        return true;
    }

    @Override
    public String headline() {
        return "Cooked Meals";
    }
    
}
