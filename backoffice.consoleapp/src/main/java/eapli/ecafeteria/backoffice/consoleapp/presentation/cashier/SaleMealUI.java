/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cashier;

import eapli.ecafeteria.application.SaleMealController;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andreiavilarinho
 */
public class SaleMealUI extends AbstractUI {
    
    private final SaleMealController theController = new SaleMealController();
    
    @Override
    protected boolean doShow() {
        try {
            return this.theController.saleMeal();
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(SaleMealUI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public String headline() {
        return "Sale Meal.";
    }
    
}
