/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.ListMealsTypeController;
import eapli.ecafeteria.domain.meals.MealsType;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Bolt
 */
public class ListMealsTypeUI extends AbstractListUI<MealsType>{

    private final ListMealsTypeController theController = new ListMealsTypeController();

    protected Controller controller() {
        return this.theController;
    }
    
    @Override
    protected Iterable<MealsType> listOfElements() {
        return this.theController.listMealsTypes();
    }

    @Override
    protected Visitor<MealsType> elementPrinter() {
        return new MealsTypePrinter();
    }

    @Override
    protected String elementName() {
        return "Meals Type";
    }
    
}
