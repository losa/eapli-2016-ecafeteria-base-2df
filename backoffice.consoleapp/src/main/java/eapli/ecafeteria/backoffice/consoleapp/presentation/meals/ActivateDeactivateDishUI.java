/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.ActivateDeactivateDishController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

/**
 *
 * @author Luís Maia
 */
public class ActivateDeactivateDishUI extends AbstractUI {

    private final ActivateDeactivateDishController theController = new ActivateDeactivateDishController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        final Iterable<Dish> allDishs = this.theController.listDishs();
        if (!allDishs.iterator().hasNext()) {
            System.out.println("There is no registered Dish");
        } else {
            final SelectWidget<Dish> selector = new SelectWidget<>(allDishs, new DishPrinter());
            selector.show();
            final Dish updtDish = selector.selectedElement();
            this.theController.changeDishState(updtDish);
        }
        return true;
    }

    @Override
    public String headline() {
        return "Activate / Deactivate Dish ";
    }
}
