/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cashier;

import eapli.ecafeteria.application.ClosePOSController;
import eapli.framework.presentation.console.AbstractUI;

/**
 *
 * @author Asus
 */
public class ClosePOSUI extends AbstractUI {

    private final ClosePOSController theController = new ClosePOSController();

    @Override
    protected boolean doShow() {
        return  this.theController.closePOS();
    }

    @Override
    public String headline() {
        return "Close Cashier.";
    }
}
