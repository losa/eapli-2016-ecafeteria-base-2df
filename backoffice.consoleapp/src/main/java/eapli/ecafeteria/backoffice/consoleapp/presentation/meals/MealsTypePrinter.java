/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealsType;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Bolt
 */
public class MealsTypePrinter implements Visitor<MealsType>{

    @Override
    public void visit(MealsType visitee) {
        System.out.printf("%-10s%-30s\n", visitee.id(), visitee.description());
    }

    @Override
    public void beforeVisiting(MealsType visitee) {
        
    }

    @Override
    public void afterVisiting(MealsType visitee) {
        
    }
    
}
