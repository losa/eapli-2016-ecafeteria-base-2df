/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;

/**
 *
 * @author Luís Maia
 */
public class RegisterDishUI extends AbstractUI {

    private final RegisterDishController theController = new RegisterDishController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        System.out.println("Select Type of Dish");
        final SelectWidget<DishType> selector = new SelectWidget<>(this.theController.listDishTypesAvaiable(),
                new DishTypePrinter());
        selector.show();
        final DishType theDishType = selector.selectedElement();
        if (theDishType != null) {
            final String name = Console.readLine("Dish Name:");
            final Double price = Console.readDouble("Dish Price:");
            final Double calorias = Console.readDouble("Dish Calories:");
            final Double sal = Console.readDouble("Dish Amount of Salt:");
            final Double gordura = Console.readDouble("Dish Amount of Fat:");
            try {
                this.theController.RegisterDishController(name, price, calorias, sal, gordura, theDishType);
            } catch (final DataIntegrityViolationException e) {
                System.out.println("That dish is already in use.");
            }
        } else {
            System.out.println("No valid option selected");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Regist new Dish";
    }

}
