/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation;


import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.DisplayCookedMealsAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.CreateMealMenuAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.RegisterMealAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.*;
import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.cafeteria.consoleapp.presentation.authz.LoginUI;
import eapli.ecafetaria.backoffice.consoleapp.presentation.mealbooking.ReservationsByDayAction;
import eapli.ecafetaria.backoffice.consoleapp.presentation.mealbooking.ReservationsByDishAction;
import eapli.ecafetaria.backoffice.consoleapp.presentation.mealbooking.ReservationsByMealAction;
import eapli.ecafetaria.backoffice.consoleapp.presentation.mealbooking.ReservationsByTypeAction;
import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.ListOrganicUnitsController;
import eapli.ecafeteria.application.LoginController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.AcceptRefuseSignupRequestAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.AddUserUI;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.DeactivateUserAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.authz.ListUsersAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria.AddOrganicUnitAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria.AddOrganicUnitUI;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria.ChangeOrganicUnitAction;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cafeteria.OrganicUnitPrinter;
import eapli.ecafeteria.backoffice.consoleapp.presentation.cashier.*;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.framework.actions.ReturnAction;
import eapli.framework.actions.ShowMessageAction;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.HorizontalMenuRenderer;
import eapli.framework.presentation.console.ListUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final int EXIT_OPTION = 0;

    // USERS
    private static final int ADD_USER_OPTION = 1;
    private static final int LIST_USERS_OPTION = 2;
    private static final int DEACTIVATE_USER_OPTION = 3;
    private static final int ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION = 4;

    // ORGANIC UNITS
    private static final int ADD_ORGANIC_UNIT_OPTION = 1;
    private static final int LIST_ORGANIC_UNIT_OPTION = 2;
    private static final int CHANGE_ORGANIC_UNIT_OPTION = 3;
    private static final int DEACTIVATE_ORGANIC_UNIT_OPTION = 4;

    // SETTINGS
    private static final int SET_KITCHEN_ALERT_LIMIT_OPTION = 1;
    private static final int SET_USER_ALERT_LIMIT_OPTION = 2;

    // DISH
    private static final int ADD_DISH_OPTION = 1;
    private static final int LIST_DISH_OPTION = 2;
    private static final int CHANGE_DISH_OPTION = 3;
    private static final int DISH_ACTIVATE_DEACTIVATE_OPTION = 4;

    //MEALS
    private static final int MEALS_TYPE_OPTION = 1;
    private static final int LIST_MEALS_OPTION = 2;
    private static final int CREATE_MEALMENU = 3;
    private static final int REGIST_MEAL = 4;
    private static final int CONSULTAR_MEALMENU = 5;

    // DISH TYPES
    private static final int DISH_TYPE_REGISTER_OPTION = 1;
    private static final int DISH_TYPE_LIST_OPTION = 2;
    private static final int DISH_TYPE_CHANGE_OPTION = 3;
    private static final int DISH_TYPE_ACTIVATE_DEACTIVATE_OPTION = 4;

    //CASHIER REGISTER
    private static final int OPEN_CASHIER_OPTION = 1;
    private static final int CLOSE_CASHIER_OPTION = 2;

    //CASHIER MEAL'S/CHARGE
    private static final int REGISTER_RESERVATION_OPTION = 1;
    private static final int SALE_MEAL_OPTION = 2;
    private static final int CHARGE_CARD_OPTION = 3;
    private static final int LIST_DISHES_OPTION = 4;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int USERS_OPTION = 2;
    private static final int ORGANIC_UNITS_OPTION = 3;
    private static final int SETTINGS_OPTION = 4;
    private static final int DISH_TYPES_OPTION = 5;
    private static final int DISH_OPTION = 6;
    private static final int MEALS_OPTION = 8;
    private static final int CASHIER_REGISTER_OPTION = 9;
    private static final int MEAL_CHARGE_OPTION = 10;

    //KITCHEN MANAGER
    private static final int EXISTENT_RESERVATIONS_OPTION = 2;
    private static final int INSERT_MEAL_INFO_OPTION = 3;
    private static final int INSERT_MEALS_NOT_SOLD_OPTION = 3;
    private static final int INSERT_MEALS_MADE_OPTION = 1;
    private static final int DISPLAY_MEALS_MADE_OPTION = 2;
    private static final int CREATE_PLAN_MENU = 5;
    private static final int SHOW_ALERTS = 6;
    
    //Plan Menu
    private static final int CREATE_PLANMENU = 1;
    private static final int REGISTER_ITEMPLAN = 2;

    //KITCHEN RESERVATIONS
    private static final int SORT_BY_DAY = 1;
    private static final int SORT_BY_TYPE = 2;
    private static final int SORT_BY_DISH = 3;
    private static final int SORT_BY_MEAL = 4;

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (AppSettings.instance().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu);
        } else {
            renderer = new VerticalMenuRenderer(menu);
        }
        return renderer.show();
    }

    @Override
    public String headline() {
        if(AppSettings.instance().session()==null){
            new LoginUI().show();
            return "";
        }else{
            return "eCAFETERIA [@" + AppSettings.instance().session().authenticatedUser().id() + "]";
        }
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        if (!AppSettings.instance().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.Administer)) {
            final Menu usersMenu = buildUsersMenu();
            mainMenu.add(new SubMenu(USERS_OPTION, usersMenu, new ShowVerticalSubMenuAction(usersMenu)));
            final Menu organicUnitsMenu = buildOrganicUnitsMenu();
            mainMenu.add(new SubMenu(ORGANIC_UNITS_OPTION, organicUnitsMenu,
                    new ShowVerticalSubMenuAction(organicUnitsMenu)));
            final Menu settingsMenu = buildAdminSettingsMenu();
            mainMenu.add(new SubMenu(SETTINGS_OPTION, settingsMenu, new ShowVerticalSubMenuAction(settingsMenu)));
        } else if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.ManageKitchen)) {

            final Menu existentReservationsMenu = buildExistentReservationsMenu();

            mainMenu.add(new SubMenu(EXISTENT_RESERVATIONS_OPTION, existentReservationsMenu,
                    new ShowVerticalSubMenuAction(existentReservationsMenu)));

            final Menu insertMealInfoMenu = buildMealInfoMenu();
            mainMenu.add(new SubMenu(INSERT_MEAL_INFO_OPTION, insertMealInfoMenu, new ShowVerticalSubMenuAction(insertMealInfoMenu)));
            
            final Menu insertPlanMenu = buildPlanMenu();
            mainMenu.add(new SubMenu(CREATE_PLAN_MENU, insertPlanMenu, new ShowVerticalSubMenuAction(insertPlanMenu)));
            
            mainMenu.add(new MenuItem(SHOW_ALERTS, "Show alerts", new ShowAlertsAction()));

        } else if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.ManageMenus)) {
            final Menu myDishTypeMenu = buildDishTypeMenu();
            mainMenu.add(new SubMenu(DISH_TYPES_OPTION, myDishTypeMenu, new ShowVerticalSubMenuAction(myDishTypeMenu)));

            final Menu myDishMenu = buildDishMenu();
            mainMenu.add(new SubMenu(DISH_OPTION, myDishMenu, new ShowVerticalSubMenuAction(myDishMenu)));

            final Menu myMealsDish = buildMealsMenu();
            mainMenu.add(new SubMenu(MEALS_OPTION, myMealsDish, new ShowVerticalSubMenuAction(myMealsDish)));

        } else if (AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.Sale)) {
            final Menu myCashierMenu = buildCashierRegisterMenu();
            mainMenu.add(new SubMenu(CASHIER_REGISTER_OPTION, myCashierMenu, new ShowVerticalSubMenuAction(myCashierMenu)));
            final Menu myCashierOptionsMenu = buildCashierOptionsMenu();
            mainMenu.add(new SubMenu(MEAL_CHARGE_OPTION, myCashierOptionsMenu, new ShowVerticalSubMenuAction(myCashierOptionsMenu)));
        }

        if (!AppSettings.instance().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }

    private Menu buildAdminSettingsMenu() {
        final Menu menu = new Menu("Settings >");

        menu.add(new MenuItem(SET_KITCHEN_ALERT_LIMIT_OPTION, "Set kitchen alert limit",
                new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(SET_USER_ALERT_LIMIT_OPTION, "Set users' alert limit",
                new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildOrganicUnitsMenu() {
        final Menu menu = new Menu("Organic units >");

        menu.add(new MenuItem(ADD_ORGANIC_UNIT_OPTION, "Add Organic Unit", new AddOrganicUnitAction()));
        menu.add(new MenuItem(LIST_ORGANIC_UNIT_OPTION, "List Organic Unit", () -> {
            // example of using the generic list ui from the framework
            new ListUI<OrganicUnit>(new ListOrganicUnitsController().listOrganicUnits(), new OrganicUnitPrinter(),
                    "Organic Unit").show();
            return false;
        }));
        menu.add(new MenuItem(CHANGE_ORGANIC_UNIT_OPTION, "Change Organic Unit", new ChangeOrganicUnitAction()));
        menu.add(new MenuItem(DEACTIVATE_ORGANIC_UNIT_OPTION, "Deactivate Organic Unit", new ChangeOrganicUnitAction()));
        
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildUsersMenu() {
        final Menu menu = new Menu("Users >");

        menu.add(new MenuItem(ADD_USER_OPTION, "Add User", () -> {
            return new AddUserUI().show();
        }));
        menu.add(new MenuItem(LIST_USERS_OPTION, "List all Users", new ListUsersAction()));
        menu.add(new MenuItem(DEACTIVATE_USER_OPTION, "Deactivate User", new DeactivateUserAction()));
        menu.add(new MenuItem(ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION, "Accept/Refuse Signup Request",
                new AcceptRefuseSignupRequestAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildDishMenu() {
        final Menu menu = new Menu("Dish >");

        menu.add(new MenuItem(ADD_DISH_OPTION, "Register new Dish", new RegisterDishAction()));
        menu.add(new MenuItem(LIST_DISH_OPTION, "List all Dish", new ListDishAction()));
        menu.add(new MenuItem(CHANGE_DISH_OPTION, "Change Dish Name", new ChangeDishAction()));
        menu.add(new MenuItem(DISH_ACTIVATE_DEACTIVATE_OPTION, "Activate/Deactivate Dish",
                new ActivateDeactivateDishAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildMealsMenu() {
        final Menu menu = new Menu("Meals >");

        menu.add(new MenuItem(MEALS_TYPE_OPTION, "Register new Type of Meals", new RegisterTypeMealsAction()));
        menu.add(new MenuItem(LIST_MEALS_OPTION, "List all Types of Meals", new ListMealsTypeAction()));
        menu.add(new MenuItem(CREATE_MEALMENU, "Creat meal menu", new CreateMealMenuAction()));
        menu.add(new MenuItem(REGIST_MEAL, "Regist meal", new RegisterMealAction()));
        menu.add(new MenuItem(CONSULTAR_MEALMENU, "Consult menu", new ListMealMenuAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildDishTypeMenu() {
        final Menu menu = new Menu("Dish Type >");

        menu.add(new MenuItem(DISH_TYPE_REGISTER_OPTION, "Register new Dish Type", new RegisterDishTypeAction()));
        menu.add(new MenuItem(DISH_TYPE_LIST_OPTION, "List all Dish Type", new ListDishTypeAction()));
        menu.add(new MenuItem(DISH_TYPE_CHANGE_OPTION, "Change Dish Type description", new ChangeDishTypeAction()));
        menu.add(new MenuItem(DISH_TYPE_ACTIVATE_DEACTIVATE_OPTION, "Activate/Deactivate Dish Type",
                new ActivateDeactivateDishTypeAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildExistentReservationsMenu() {

        final Menu menu = new Menu("List Reservations >");

        menu.add(new MenuItem(SORT_BY_DAY, "Sort Reservations by Day", new ReservationsByDayAction()));
        menu.add(new MenuItem(SORT_BY_TYPE, "Sort Reservations by Type", new ReservationsByTypeAction()));
        menu.add(new MenuItem(SORT_BY_DISH, "Sort Reservations by Dish", new ReservationsByDishAction()));
        menu.add(new MenuItem(SORT_BY_MEAL, "Sort Reservations by Meal", new ReservationsByMealAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

    private Menu buildPlanMenu() {

        final Menu menu = new Menu("PlanMenu >");

        menu.add(new MenuItem(CREATE_PLANMENU, "Create Plan Menu", new CreatePlanMenuAction()));
        menu.add(new MenuItem(REGISTER_ITEMPLAN, "Register Plan", new RegisterItemPlanAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }
    
    private Menu buildCashierRegisterMenu() {
        final Menu menu = new Menu("Cashier Menu >");

        menu.add(new MenuItem(OPEN_CASHIER_OPTION, "Open cashier", new OpenPOSAction()));
        menu.add(new MenuItem(CLOSE_CASHIER_OPTION, "Close cashier", new ClosePOSAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildCashierOptionsMenu() {
        final Menu menu = new Menu("Data >");

        menu.add(new MenuItem(REGISTER_RESERVATION_OPTION, "Register Reservation", new DeliveryReservationAction()));
        menu.add(new MenuItem(SALE_MEAL_OPTION, "Sale meal", new SaleMealAction()));
        menu.add(new MenuItem(CHARGE_CARD_OPTION, "Charge Card", new ChargeCardAction()));
        menu.add(new MenuItem(LIST_DISHES_OPTION, "List Dishes", new ListAvailableDishesAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }

    private Menu buildMealInfoMenu() {
        final Menu menu = new Menu("Meal Info >");

        //add menus.
        menu.add(new MenuItem(INSERT_MEALS_MADE_OPTION, "Insert Nr. of Cooked Meals", new RegisterMealsMadeAction()));
        menu.add(new MenuItem(DISPLAY_MEALS_MADE_OPTION, "Display Nr. of Cooked Meals", new DisplayCookedMealsAction()));
        //menu.add(new MenuItem(INSERT_MEALS_NOT_SOLD_OPTION, "Insert Nr. of Meals Not Sold", new RegisterMealsNotSoldAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }
}
