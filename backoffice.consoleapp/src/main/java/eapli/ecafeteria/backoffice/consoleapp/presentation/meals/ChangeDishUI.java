/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.ChangeDishController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;

/**
 *
 * @author Luís Maia
 */
public class ChangeDishUI extends AbstractUI {

    private final ChangeDishController theController = new ChangeDishController();

    protected Controller controller() {
        return this.theController;
    }

    public ChangeDishUI() {
    }

    @Override
    protected boolean doShow() {
        final Iterable<Dish> dish = this.theController.listDish();
        final SelectWidget<Dish> selector = new SelectWidget<>(dish, new DishPrinter());
        selector.show();
        final Dish theDish = selector.selectedElement();
        if (theDish != null) {
            final String newDishName = Console
                    .readLine("Enter new name for " + theDish.name() + ": ");
            this.theController.changeDishName(theDish, newDishName);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Change Dish";
    }

}
