/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.cashier;

import eapli.framework.actions.Action;

/**
 *
 * @author Asus
 */
public class OpenPOSAction implements Action{

    @Override
    public boolean execute() {
        return new OpenPOSUI().show();
    }
    
}
