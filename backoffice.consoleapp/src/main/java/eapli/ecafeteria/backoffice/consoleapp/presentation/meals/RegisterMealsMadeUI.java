/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterMealInformationController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;

/**
 *
 * @author Ardyvee
 */
public class RegisterMealsMadeUI extends AbstractUI {

    private final RegisterMealInformationController controller = new RegisterMealInformationController();

    @Override
    protected boolean doShow() {
        System.out.println("Select the plan:");
        final Iterable<MealPlan> plans = controller.allMealPlans();
        final SelectWidget<MealPlan> selector = new SelectWidget<>(plans, new MealPlanPrinter());
        selector.show();
        final MealPlan plan = selector.selectedElement();
        
        if(plan == null) {
            return true;
        }

        System.out.println("Select the meal:");
        final SelectWidget<MealPlanItem> select = new SelectWidget<>(this.controller.allMenuPlanItemsOfPlan(plan), new MealPlanItemPrinter());
        select.show();
        final MealPlanItem item = select.selectedElement();

        if (item != null) {
            int number = -1;
            do {
                final String newDescription = Console
                        .readLine("Insert how many meals were made: ");
                try {
                    number = Integer.parseInt(newDescription);
                } catch (Exception ex) {
                    continue;
                }
            } while (number < 0);
            this.controller.mealsCooked(number, item);
        }
        return true;
    }

    @Override
    public String headline() {
        return "Register Meals Made";
    }

}
