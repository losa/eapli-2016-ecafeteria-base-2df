/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.ListMealMenuController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.framework.application.Controller;
import eapli.framework.domain.TimePeriod2;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Bolt
 */
public class ListMealMenuUI extends AbstractUI {

    private final ListMealMenuController theController = new ListMealMenuController();
    private List<Meal> lstMeals;
    private TimePeriod2 tp = new TimePeriod2(); 
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        
        final SelectWidget<MealMenu> selectorMenu = new SelectWidget<>(this.theController.listMenus(),
                new MealMenuPrinter());
        selectorMenu.show();
        final MealMenu theMenu = selectorMenu.selectedElement();
        
        lstMeals = (List)this.theController.listMeals();
        
        for(Meal meal: lstMeals) {
            if(meal.mealMenu().descricao().equals(theMenu.descricao())) {
                String dia = tp.converter(meal.day());
                System.out.println("O prato é " + meal.dish().name() + " na ementa "  + meal.mealMenu().descricao() + " no dia " + dia);
            }
        }

        return false;
    }

    @Override
        public String headline() {
        return "Consult MealMenu";
    }

    
    
    
}
