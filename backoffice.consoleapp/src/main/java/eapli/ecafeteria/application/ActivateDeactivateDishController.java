/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

/**
 *
 * @author Luís Maia
 */
public class ActivateDeactivateDishController implements Controller {

    public Iterable<Dish> listDishs() {
        return new ListDishService().allDish();
    }

    public void changeDishState(Dish dish) {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        dish.changeDishState();
        final DishRepository dishTypeRepository = PersistenceContext.repositories().dish();
        dishTypeRepository.save(dish);
    }
}
