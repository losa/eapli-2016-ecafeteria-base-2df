/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.cafeteria.consoleapp.presentation.authz;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.ChangePasswordCashierController;
import eapli.ecafeteria.application.LoginController;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.UnableToAuthenticateException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RP
 */
public class ChangePasswordUI extends AbstractUI {
    private final ChangePasswordCashierController theController = new  ChangePasswordCashierController();
    private final AppSettings app = AppSettings.instance();
    /*
     * private final Logger logger = LoggerFactory.getLogger(LoginUI.class);
     * 
     * public Logger getLogger() { return logger; }
     */


    @Override
    protected boolean doShow() {
        try {
            final String password = Console.readLine(" new Password:");
            
            
            int x =this.theController.changePass(password);
            if(x == 1) {
                System.out.println("Password demasiado pequena, no minimo 6 caracteres");
                return false;
            }else{
                System.out.println("Password Alterada");
                this.theController.saveChanges(password, app.session().authenticatedUser().username().toString());
//                AppSettings.instance().session().a;
            }
            
            return true;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ChangePasswordUI.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Escreva algo");
        }
       
        
       return false;
    }

    @Override
    public String headline() {
        return "Change Password";
    }
}
