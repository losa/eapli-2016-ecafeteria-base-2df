/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.cafeteria.consoleapp.presentation.authz;

import eapli.ecafeteria.application.LogoutController;
import eapli.ecafeteria.domain.authz.UnableToAuthenticateException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author Asus
 */
public class LogoutUI extends AbstractUI{

    private final LogoutController theController = new LogoutController();
    
    @Override
    protected boolean doShow() {
        return this.theController.logout();        
    }

    @Override
    public String headline() {
        return "Login";
    }
    
}
