/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.CreateMealMenuController;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luís Maia
 */
public class MealMenuBootstrap implements Action {

    @Override
    public boolean execute() {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            register("eapli", DateTime.dateToCalendar(df.parse("01-05-2016")), DateTime.dateToCalendar(df.parse("10-06-2016")));
        } catch (ParseException ex) {
            Logger.getLogger(MealMenuBootstrap.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(MealMenuBootstrap.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private void register(String descricao, Calendar begin, Calendar end) throws DataIntegrityViolationException {
 
        final CreateMealMenuController controller = new CreateMealMenuController();
       // try {
            controller.CreateMealMenuController(descricao, begin, end);
        /*} catch (final Exception e) {
            System.out.println("erro:");
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }*/
    }
}
