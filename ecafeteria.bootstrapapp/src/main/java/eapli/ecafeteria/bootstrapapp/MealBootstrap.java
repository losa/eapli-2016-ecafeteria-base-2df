/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.RegisterMealsController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealMenu;
import eapli.ecafeteria.domain.meals.MealsType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealMenuRepository;
import eapli.ecafeteria.persistence.MealsTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.util.DateTime;
import static java.awt.SystemColor.menu;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luís Maia
 */
public class MealBootstrap implements Action {

    @Override
    public boolean execute() {
        int contDish = 0;
        final DishRepository repoDish = PersistenceContext.repositories().dish();
        final MealsTypeRepository repoMealType = PersistenceContext.repositories().mealsType();
        final MealMenuRepository repo = PersistenceContext.repositories().mealMenu();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        Iterator<Dish> itDish = repoDish.all().iterator();
        Iterator<MealsType> itMtype = repoMealType.all().iterator();
        Iterator<MealMenu> itMMenu = repo.all().iterator();

        MealMenu mm = itMMenu.next();
        MealsType mtype = itMtype.next();

        while (contDish < 3) {
            if (itDish.hasNext()) {
                switch (contDish) {
                    case 0: {
                        try {
                            register(itDish.next(), mtype, DateTime.dateToCalendar(df.parse("02-06-2016")), mm);
                        } catch (ParseException ex) {
                            Logger.getLogger(MealBootstrap.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                    case 1: {
                        try {
                            register(itDish.next(), mtype, DateTime.dateToCalendar(df.parse("03-06-2016")), mm);
                        } catch (ParseException ex) {
                            Logger.getLogger(MealBootstrap.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                    case 2: {
                        try {
                            register(itDish.next(), mtype, DateTime.dateToCalendar(df.parse("04-06-2016")), mm);
                        } catch (ParseException ex) {
                            Logger.getLogger(MealBootstrap.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                }
                contDish++;
            } else {
                System.out.println("No more dishes in repository.");
            }
        }
        return contDish == 3;
    }

    private void register(Dish theDish, MealsType theMealsType, Calendar day, MealMenu theMenu) {
        final RegisterMealsController controller = new RegisterMealsController();
        try {
            controller.RegisterMealsController(theDish, theMealsType, day, theMenu);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }
}
