/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.Reservation;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.ReservationsRepository;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bruno
 */
public class ReservationsBootstrap implements Action {

    @Override
    public boolean execute() {
        CafeteriaUserRepository rep = PersistenceContext.repositories().cafeteriaUsers();
        CafeteriaUser cu = rep.findBySystemUser(AppSettings.instance().session().authenticatedUser());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        int cont = 0;
        final MealRepository repo = PersistenceContext.repositories().meal();
        Iterator<Meal> iterator = repo.all().iterator();
        while (cont < 3) {
            if (iterator.hasNext()) {
                Meal m = iterator.next();
                switch (cont) {
                    case 0: {
                    try {
                        register(m, DateTime.dateToCalendar(df.parse("02-06-2016")), cu);
                    } catch (DataIntegrityViolationException ex) {
                        Logger.getLogger(ReservationsBootstrap.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ParseException ex) {
                        Logger.getLogger(ReservationsBootstrap.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                    break;
                    case 1: {
                    try {
                        register(m, DateTime.dateToCalendar(df.parse("03-06-2016")), cu);
                    } catch (DataIntegrityViolationException ex) {
                        Logger.getLogger(ReservationsBootstrap.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ParseException ex) {
                        Logger.getLogger(ReservationsBootstrap.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                    break;
                    case 2: {
                    try {
                        register(m, DateTime.dateToCalendar(df.parse("04-06-2016")), cu);
                    } catch (DataIntegrityViolationException ex) {
                        Logger.getLogger(ReservationsBootstrap.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ParseException ex) {
                        Logger.getLogger(ReservationsBootstrap.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
                cont++;
            } else {
                System.out.println("No more meals in repository.");
                break;
            }
        }
        
        return cont==3;
    }

    public void register(Meal meal, Calendar date, CafeteriaUser user) throws DataIntegrityViolationException, ParseException {

        ReservationsRepository res = PersistenceContext.repositories().reservations();
        Reservation r = new Reservation(meal, date, user);
        res.add(r);
    }
}
