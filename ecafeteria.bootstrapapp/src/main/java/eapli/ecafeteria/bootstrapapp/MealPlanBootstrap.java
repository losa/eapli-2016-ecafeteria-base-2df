/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.CreatePlanMenuController;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.framework.actions.Action;
import eapli.util.DateTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andreiavilarinho
 */
public class MealPlanBootstrap implements Action {

    @Override
    public boolean execute() {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            register("eapli-teste", DateTime.dateToCalendar(df.parse("01-05-2016")),
                    DateTime.dateToCalendar(df.parse("10-06-2016")));
        } catch (ParseException ex) {
            Logger.getLogger(MealPlanBootstrap.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    private void register(String descricao, Calendar begin, Calendar end) {
        final CreatePlanMenuController controller = new CreatePlanMenuController();
        MealPlan mp = new MealPlan(begin, end, descricao);
        try {
            controller.CreateMealPlanController(mp);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }
    
}
