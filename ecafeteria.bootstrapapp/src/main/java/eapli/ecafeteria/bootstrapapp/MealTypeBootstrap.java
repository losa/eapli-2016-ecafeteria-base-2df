/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.RegisterTypeMealsController;
import eapli.framework.actions.Action;

/**
 *
 * @author Luís Maia
 */
public class MealTypeBootstrap implements Action {

       @Override
    public boolean execute() {
        register("Lunch", "Almoco");
        register("Dinner", "Jantar");
        return false;
    }

    private void register(String name,String descricao) {
        final RegisterTypeMealsController controller = new RegisterTypeMealsController();
        try {
            controller.registerMealsType(name,descricao);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }
    
}
