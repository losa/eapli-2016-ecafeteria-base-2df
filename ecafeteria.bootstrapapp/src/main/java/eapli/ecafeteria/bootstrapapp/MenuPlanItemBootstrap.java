/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.RegisterItemPlanController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import java.util.Iterator;

/**
 *
 * @author andreiavilarinho
 */
public class MenuPlanItemBootstrap implements Action {

    @Override
    public boolean execute() {
        final MealRepository repoMeal = PersistenceContext.repositories().meal();
        final MealPlanRepository repoMealPlan = PersistenceContext.repositories().mealPlan();

        Iterator<Meal> itMeal = repoMeal.all().iterator();
        Iterator<MealPlan> itMPlan = repoMealPlan.all().iterator();
        Meal m = itMeal.next();
        MealPlan mp = itMPlan.next();

        register(m, mp);
        return false;
    }

    private void register(Meal theMeal, MealPlan thePlan) {
        final RegisterItemPlanController controller = new RegisterItemPlanController();
        try {
            MealPlanItem mpi = new MealPlanItem(theMeal, thePlan);
            mpi.avaiableQtt(10);
            mpi.cookedQtt(10);
            mpi.delieredQtt(10);
            mpi.intendedQtt(10);
            controller.RegisterItemPlanController(mpi);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }

}
