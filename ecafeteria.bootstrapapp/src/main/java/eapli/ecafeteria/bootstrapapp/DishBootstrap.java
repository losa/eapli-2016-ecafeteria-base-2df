/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import java.util.Iterator;

/**
 *
 * @author Luís Maia
 */
public class DishBootstrap implements Action {

    @Override
    public boolean execute() {
        int cont = 0;
        final DishTypeRepository repo = PersistenceContext.repositories().dishTypes();
        Iterable<DishType> it = repo.activeDishTypes();
        Iterator<DishType> iterator = it.iterator();
        while (cont < 3) {
            if (iterator.hasNext()) {
                DishType d = iterator.next();
                switch (cont) {
                    case 0:
                        register("Almôndegas de soja", 3.6, 256.0, 0.3, 0.0, d);
                        break;
                    case 1:
                        register("Sardinhas", 3.6, 260.0, 0.654, 10.0, d);
                        break;
                    case 2:
                        register("Cachorro", 3.8, 359.0, 0.745, 13.0, d);
                        break;
                }
                cont++;
            } else {
                System.out.println("No more dish types in repository.");
            }
        }
        
        return cont==3;
    }

    private void register(String name, Double price, Double calorias, Double sal, Double gordura, DishType dishType) {
        final RegisterDishController controller = new RegisterDishController();
        try {
            controller.RegisterDishController(name, price, calorias, sal, gordura, dishType);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }

}
